#!/bin/bash
################################################
# Global Variables
################################################
export PATH=$(pwd)/node_modules/.bin:$PATH

################################################
# Aux functions
################################################
log() {
  echo ">> [eslint-config] " $@
}

################################################
# Bake Recipes
################################################
###%
#@ bake deps
##
## This recipe install all necessary dependencies.
##
bake-deps() {
  log "Installing dependencies"
  yarn install
}

###%
#@ bake init
##
## This recipe going to initialize this app/service.
##
bake-init() {
  log "Initializing"
  bake-deps
  bake-setup
}

###%
#@ bake lint
##
## This recipe going to lint the code.
##
bake-lint() {
  eslint ./check-test --ext .ts --ext .tsx --ext .js --ext .jsx
}

###%
#@ bake publish
##
## This recipe going to publish all packages in this workspace.
##
bake-publish() {
  log "Publishing"
  semantic-release --debug --repository-url https://oauth2:${GL_TOKEN}@gitlab.com/nosebit/eslint-config.git
}

###%
#@ bake setup
##
## This recipe going to setup this context.
##
bake-setup() {
  log "Setting up"
}

###%
#@ bake update
##
## This recipe going to update this repo.
##
bake-update() {
  log "Updating"

  local DEFAULT_BRANCH="master"
  local CURRENT_BRANCH=$(git -C $d branch | grep \* | cut -d ' ' -f2)
  local STASHED="false"

  echo "CURRENT_BRANCH=$CURRENT_BRANCH"

  # If we have uncommited changes let's stash them
  if [ -n "$(git status --porcelain)" ]; then
    git stash
    STASHED="true"
  fi

  echo "STASHED=$STASHED"

  if [ "$CURRENT_BRANCH" != "$DEFAULT_BRANCH" ]; then
    git checkout $DEFAULT_BRANCH
  fi

  git pull --rebase
  git checkout $CURRENT_BRANCH

  if [ "$STASHED" = "true" ]; then
    git stash pop
  fi
}

################################################
# Main
################################################
# Process subcommand
subcommand=$1

case $subcommand in
  "help" |  "-h" | "--help")
  bake-help
  ;;
  *)
    shift
    bake-${subcommand} $@
    EXIT_CODE=$(echo $?)

    if [ "$EXIT_CODE" = 127 ]; then
        echo "Error: '$subcommand' is not a known subcommand." >&2
        echo "       Run '$ProgName --help' for a list of known subcommands." >&2
    fi

    exit $EXIT_CODE
  ;;
esac
