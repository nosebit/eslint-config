const path = require("path");
const eslintConfig = require("./index");

module.exports = eslintConfig.configBuild({
  configBuilderOptions: {
    tsConfigFile: path.resolve(__dirname, "tsconfig.json"),
  }
});
