/* eslint-disable */

const builtinModules = require('builtin-modules');
const lodash = require("lodash");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

//#####################################################
// Eslint Rules
//#####################################################
const eslintRules = (opts) => ({
  /**
   * This rule warns if setters are defined without getters.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/accessor-pairs.md
   */
  "accessor-pairs": [
    "error"
  ],

  /**
   * This rule enforces line breaks after opening and before closing array brackets.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/array-bracket-newline.md
   */
  "array-bracket-newline": [
    "error",
    "consistent"
  ],

  /**
   * This rule enforces consistent spacing inside array brackets.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/array-bracket-spacing.md
   */
  "array-bracket-spacing": [
    "error",
    "never"
  ],

  /**
   * Array has several methods for filtering, mapping, and folding. If we forget
   * to write return statement in a callback of those, it's probably a mistake.
   * If you don't want to use a return or don't need the returned results,
   * consider using .forEach instead.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/array-callback-return.md
   */
  "array-callback-return": [
    "error"
  ],

  /**
   * This rule enforces line breaks between array elements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/array-element-newline.md
   */
  "array-element-newline": [
    "error",
    "consistent"
  ],

  /**
   * This rule can enforce or disallow the use of braces around arrow function body.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/arrow-body-style.md
   */
  "arrow-body-style": [
    "error",
    "as-needed",
    {
      "requireReturnForObjectLiteral": false
    }
  ],

  /**
   * Arrow functions can omit parentheses when they have exactly one parameter.
   * In all other cases the parameter(s) must be wrapped in parentheses.
   * This rule enforces the consistent use of parentheses in arrow functions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/arrow-parens.md
   */
  "arrow-parens": [
    "error",
    "always"
  ],

  /**
   * This rule normalize style of spacing before/after an arrow function's arrow(=>).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/arrow-spacing.md
   */
  "arrow-spacing": [
    "error",
    {
      before: true,
      after: true,
    },
  ],

  /**
   * The block-scoped-var rule generates warnings when variables are used outside
   * of the block in which they were defined. This emulates C-style block scope.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/block-scoped-var.md
   */
  "block-scoped-var": [
    "error",
  ],

  /**
   * This rule enforces consistent spacing inside an open block token and the
   * next token on the same line. This rule also enforces consistent spacing
   * inside a close block token and previous token on the same line.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/block-spacing.md
   */
  "block-spacing": [
    "error",
    "always",
  ],

  /**
   * Brace style is closely related to indent style in programming and describes
   * the placement of braces relative to their control statement and body.
   * There are probably a dozen, if not more, brace styles in the world.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/brace-style.md
   */
  "brace-style": [
    "off",
    "1tbs"
  ],

  /**
   * This rule is aimed at ensuring that callbacks used outside of the main
   * function block are always part-of or immediately preceding a return statement.
   * This rule decides what is a callback based on the name of the function
   * being called.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/callback-return.md
   */
  "callback-return": [
    "error",
  ],

  /**
   * This rule looks for any underscores (_) located within the source code.
   * It ignores leading and trailing underscores and only checks those in the
   * middle of a variable name. If ESLint decides that the variable is a
   * constant (all uppercase), then no warning will be thrown. Otherwise, a
   * warning will be thrown. This rule only flags definitions and assignments
   * but not function calls. In case of ES6 import statements, this rule only
   * targets the name of the variable that will be imported into the local
   * module scope.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/camelcase.md
   */
  "camelcase": [
    "off",
    {
      properties: "always",
      ignoreDestructuring: false,
    },
  ],

  /**
   * This rule aims to enforce a consistent style of comments across your
   * codebase, specifically by either requiring or disallowing a capitalized
   * letter as the first word character in a comment. This rule will not issue
   * warnings when non-cased letters are used.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/capitalized-comments.md
   */
  "capitalized-comments": [
    "error",
    "always",
  ],

  /**
   * This rule is aimed to flag class methods that do not use this.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/class-methods-use-this.md
   */
  "class-methods-use-this": [
    "off",
  ],

  /**
   * This rule enforces consistent use of trailing commas in object and array
   * literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/comma-dangle.md
   */
  "comma-dangle": [
    "error",
    "always-multiline"
  ],

  /**
   * This rule enforces consistent spacing before and after commas in variable
   * declarations, array literals, object literals, function parameters, and
   * sequences.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/comma-spacing.md
   */
  "comma-spacing": [
    "error",
    {
      before: false,
      after: true,
    },
  ],

  /**
   * This rule enforce consistent comma style in array literals, object literals,
   * and variable declarations.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/comma-style.md
   */
  "comma-style": [
    "error",
    "last",
  ],

  /**
   * This rule is aimed at reducing code complexity by capping the amount of
   * cyclomatic complexity allowed in a program. As such, it will warn when
   * the cyclomatic complexity crosses the configured threshold (default is 20)
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/complexity.md
   */
  "complexity": [
    "error",
    {
      max: 20,
    },
  ],

  /**
   * This rule enforces consistent spacing inside computed property brackets.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/computed-property-spacing.md
   */
  "computed-property-spacing": [
    "error",
    "never",
  ],

  /**
   * This rule requires return statements to either always or never specify
   * values.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/consistent-return.md
   */
  "consistent-return": [
    "error"
  ],

  /**
   * This rule enforces two things about variables with the designated alias
   * names for this.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/consistent-this.md
   */
  "consistent-this": [
    "error",
    "self",
  ],

  /**
   * This rule is aimed to flag invalid/missing super() calls.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/constructor-super.md
   */
  "constructor-super": [
    "error",
  ],

  /**
   * This rule is aimed at preventing bugs and increasing code clarity by ensuring
   * that block statements are wrapped in curly braces. It will warn when it
   * encounters blocks that omit curly braces.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/curly.md
   */
  "curly": [
    "error",
    "all",
  ],

  /**
   * This rule aims to require default case in switch statements. You may
   * optionally include a // no default after the last case if there is no
   * default case. The comment may be in any desired case, such as // No
   * Default.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/default-case.md
   */
  "default-case": [
    "error",
  ],

  /**
   * This rule aims to enforce newline consistency in member expressions.
   * This rule prevents the use of mixed newlines around the dot in a member
   * expression.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/dot-location.md
   */
  "dot-location": [
    "error",
    "property",
  ],

  /**
   * This rule is aimed at maintaining code consistency and improving code
   * readability by encouraging use of the dot notation style whenever possible.
   * As such, it will warn when it encounters an unnecessary use of square-bracket
   * notation.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/dot-notation.md
   */
  "dot-notation": [
    "error",
    {
      allowKeywords: true,
    },
  ],

  /**
   * This rule enforces at least one newline (or absence thereof) at the end of
   * non-empty files.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/eol-last.md
   */
  "eol-last": [
    "error",
    "always",
  ],

  /**
   * This rule is aimed at eliminating the type-unsafe equality operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/eqeqeq.md
   */
  "eqeqeq": [
    "error",
    "always",
  ],

  /**
   * A for loop with a stop condition that can never be reached, such as one
   * with a counter that moves in the wrong direction, will run infinitely.
   * While there are occasions when an infinite loop is intended, the convention
   * is to construct such loops as while loops. More typically, an infinite for
   * loop is a bug.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/for-direction.md
   */
  "for-direction": [
    "error",
  ],

  /**
   * This rule requires or disallows spaces between the function name and the
   * opening parenthesis that calls it.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/func-call-spacing.md
   */
  "func-call-spacing": [
    "off",
    "never",
  ],

  /**
   * This rule requires function names to match the name of the variable or
   * property to which they are assigned.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/func-name-matching.md
   */
  "func-name-matching": [
    "error",
  ],

  /**
   * This rule can enforce or disallow the use of named function expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/func-names.md
   */
  "func-names": [
    "error",
    "as-needed",
  ],

  /**
   * This rule enforces a particular type of function style throughout a
   * JavaScript file, either declarations or expressions. You can specify which
   * you prefer in the configuration.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/func-style.md
   */
  "func-style": [
    "error",
    "declaration",
    {
      allowArrowFunctions: true,
    },
  ],

  /**
   * This rule enforces consistent line breaks inside parentheses of function parameters or arguments.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/function-paren-newline.md
   */
  "function-paren-newline": [
    "error",
    "consistent",
  ],

  /**
   * This rule aims to enforce spacing around the * of generator functions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/generator-star-spacing.md
   */
  "generator-star-spacing": [
    "error",
    {
      before: false,
      after: true,
    },
  ],

  /**
   * This rule enforces that a return statement is present in property getters.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/getter-return.md
   */
  "getter-return": [
    "off",
  ],

  /**
   * This rule requires all calls to require() to be at the top level of the
   * module, similar to ES6 import and export statements, which also can occur
   * only at the top level.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/global-require.md
   */
  "global-require": [
    "error",
  ],

  /**
   * This rule is aimed at preventing unexpected behavior that could arise from
   * using a for in loop without filtering the results in the loop. As such, it
   * will warn when for in loops do not filter their results with an if statement.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/guard-for-in.md
   */
  "guard-for-in": [
    "error",
  ],

  /**
   * This rule expects that when you're using the callback pattern in Node.js
   * you'll handle the error.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/handle-callback-err.md
   */
  "handle-callback-err": [
    "error",
  ],

  /**
   * This rule disallows specified identifiers in assignments and function definitions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/id-blacklist.md
   */
  "id-blacklist": [
    "error",
    "err",
    "cb",
  ],

  /**
   * This rule enforces a minimum and/or maximum identifier length convention.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/id-length.md
   */
  "id-length": [
    "error",
    {
      min: 2,
      properties: "always",
      exceptions: ["_", "i", "j", "k"],
    },
  ],

  /**
   * This rule requires identifiers in assignments and function definitions to
   * match a specified regular expression.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/id-match.md
   */
  "id-match": [
    "off",
  ],

  /**
   * This rule aims to enforce a consistent location for an arrow function
   * containing an implicit return.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/implicit-arrow-linebreak.md
   */
  "implicit-arrow-linebreak": [
    "off",
  ],

  /**
   * This rule enforces a consistent indentation style.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/indent-legacy.md
   */
  "indent": [
    "off",
    2,
    {
      SwitchCase: 1,
      MemberExpression: 1,
    }
  ],

  /**
   * This rule is aimed at enforcing or eliminating variable initializations
   * during declaration.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/init-declarations.md
   */
  "init-declarations": [
    "off",
  ],

  /**
   * This rule enforces the consistent use of either double or single quotes in
   * JSX attributes.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/jsx-quotes.md
   */
  "jsx-quotes": [
    "error",
    "prefer-double",
  ],

  /**
   * This rule enforces consistent spacing between keys and values in object
   * literal properties. In the case of long lines, it is acceptable to add a
   * new line wherever whitespace is allowed.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/key-spacing.md
   */
  "key-spacing": [
    "error",
    {
      beforeColon: false,
      afterColon: true,
      mode: "strict",
    },
  ],

  /**
   * This rule enforces consistent spacing around keywords and keyword-like tokens
   * (as, async, etc.).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/keyword-spacing.md
   */
  "keyword-spacing": [
    "error",
    {
      before: true,
      after: true,
    },
  ],

  /**
   * This rule enforces consistent position of line comments.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/line-comment-position.md
   */
  "line-comment-position": [
    "error",
    "above",
  ],

  /**
   * This rule enforces consistent line endings independent of operating system,
   * VCS, or editor used across your codebase.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/linebreak-style.md
   */
  "linebreak-style": [
    "error",
    "unix",
  ],

  /**
   * This rule requires empty lines before and/or after comments.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/lines-around-comment.md
   */
  "lines-around-comment": [
    "error",
    {
      afterLineComment: false,
      beforeLineComment: true,
      beforeBlockComment: true,
      afterBlockComment: false,
      allowClassStart: true,
      allowClassEnd: false,
      allowBlockStart: true,
      allowBlockEnd: false,
      allowObjectStart: true,
      allowObjectEnd: false,
      allowArrayStart: true,
      allowArrayEnd: false,
    }
  ],

  /**
   * This rule improves readability by enforcing lines between class members.
   * It will not check empty lines before the first member and after the last
   * member, since that is already taken care of by padded-blocks.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/lines-between-class-members.md
   */
  "lines-between-class-members": [
    "error",
    "always",
  ],

  /**
   * Files containing multiple classes can often result in a less navigable and
   * poorly structured codebase. Best practice is to keep each file limited to
   * a single responsibility.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-classes-per-file.md
   */
  "max-classes-per-file": [
    "error",
    2,
  ],

  /**
   * This rule enforces a maximum depth that blocks can be nested to reduce
   * code complexity.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-depth.md
   */
  "max-depth": [
    "error",
    4,
  ],

  /**
   * This rule enforces a maximum line length to increase code readability
   * and maintainability.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-len.md
   */
  "max-len": [
    "error",
    {
      code: 80,
      tabWidth: 2,
      ignoreComments: true,
      ignoreUrls: true,
      ignoreRegExpLiterals: true,
    }
  ],

  /**
   * This rule enforces a maximum number of lines per function, in order to aid
   * in maintainability and reduce complexity.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-lines-per-function.md
   */
  "max-lines-per-function": [
    "off",
    {
      max: 20,
      skipBlankLines: true,
      skipComments: true,
    }
  ],

  /**
   * This rule enforces a maximum number of lines per file, in order to aid in
   * maintainability and reduce complexity.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-lines.md
   */
  "max-lines": [
    "off",
  ],

  /**
   * This rule enforces a maximum depth that callbacks can be nested to increase
   * code clarity.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-nested-callbacks.md
   */
  "max-nested-callbacks": [
    "error",
    3,
  ],

  /**
   * This rule enforces a maximum number of parameters allowed in function definitions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-params.md
   */
  "max-params": [
    "error",
    {
      max: 6,
    },
  ],

  /**
   * This rule enforces a maximum number of statements allowed per line.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-statements-per-line.md
   */
  "max-statements-per-line": [
    "error",
    {
      max: 1,
    },
  ],

  /**
   * This rule enforces a maximum number of statements allowed in function blocks.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/max-statements.md
   */
  "max-statements": [
    "off",
  ],

  /**
   * This rule aims to enforce a particular style for multiline comments.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/multiline-comment-style.md
   */
  "multiline-comment-style": [
    "off",
  ],

  /**
   * This rule enforces or disallows newlines between operands of a ternary expression.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/multiline-ternary.md
   */
  "multiline-ternary": [
    "error",
    "always-multiline",
  ],

  /**
   * This rule requires constructor names to begin with a capital letter.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/new-cap.md
   */
  "new-cap": [
    opts.platform === "VUE" ? "off" : "error",
    {
      newIsCap: true,
      capIsNew: true,
      properties: true,
    },
  ],

  /**
   * This rule requires parentheses when invoking a constructor with no arguments
   * using the new keyword in order to increase code clarity.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/new-parens.md
   */
  "new-parens": [
    "error",
  ],

  /**
   * This rule requires a newline after each call in a method chain or deep member
   * access. Computed property accesses such as instance[something] are excluded.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/newline-per-chained-call.md
   */
  "newline-per-chained-call": [
    "off",
    {
      ignoreChainWithDepth: 2,
    },
  ],

  /**
   * This rule is aimed at catching debugging code that should be removed and
   * popup UI elements that should be replaced with less obtrusive, custom UIs.
   * As such, it will warn when it encounters alert, prompt, and confirm function
   * calls which are not shadowed.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-alert.md
   */
  "no-alert": [
    "error",
  ],

  /**
   * This rule disallows Array constructors.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-array-constructor.md
   */
  "no-array-constructor": [
    "error",
  ],

  /**
   * This rule aims to disallow async Promise executor functions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-async-promise-executor.md
   */
  "no-async-promise-executor": [
    "error",
  ],

  /**
   * This rule disallows the use of await within loop bodies.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-await-in-loop.md
   */
  "no-await-in-loop": [
    "error",
  ],

  /**
   * This rule disallows bitwise operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-bitwise.md
   */
  "no-bitwise": [
    "error",
  ],

  /**
   * This rule disallows calling and constructing the Buffer() constructor.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-buffer-constructor.md
   */
  "no-buffer-constructor": [
    "error",
  ],

  /**
   * This rule is aimed at discouraging the use of deprecated and sub-optimal
   * code, but disallowing the use of arguments.caller and arguments.callee.
   * As such, it will warn when arguments.caller and arguments.callee are used.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-caller.md
   */
  "no-caller": [
    "error",
  ],

  /**
   * This rule aims to prevent access to uninitialized lexical bindings as
   * well as accessing hoisted functions across case clauses.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-case-declarations.md
   */
  "no-case-declarations": [
    "error",
  ],

  /**
   * This rule is aimed to flag modifying variables of class declarations.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-class-assign.md
   */
  "no-class-assign": [
    "error",
  ],

  /**
   * The rule should warn against code that tries to compare against -0,
   * since that will not work as intended. That is, code like x === -0 will pass
   * for both +0 and -0. The author probably intended Object.is(x, -0).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-compare-neg-zero.md
   */
  "no-compare-neg-zero": [
    "error",
  ],

  /**
   * This rule disallows ambiguous assignment operators in test conditions of
   * if, for, while, and do...while statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-cond-assign.md
   */
  "no-cond-assign": [
    "error",
  ],

  /**
   * Arrow functions (=>) are similar in syntax to some comparison operators
   * (>, <, <=, and >=). This rule warns against using the arrow function
   * syntax in places where it could be confused with a comparison operator.
   * Even if the arguments of the arrow function are wrapped with parens, this
   * rule still warns about it unless allowParens is set to true.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-confusing-arrow.md
   */
  "no-confusing-arrow": [
    "off",
  ],

  /**
   * This rule disallows calls to methods of the console object.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-console.md
   */
  "no-console": [
    "error",
  ],

  /**
   * This rule is aimed to flag modifying variables that are declared using const keyword.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-const-assign.md
   */
  "no-const-assign": [
    "error",
  ],

  /**
   * This rule disallows constant expressions in the test condition of if, for,
   * while, do...while or ternary.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-constant-condition.md
   */
  "no-constant-condition": [
    "error",
    {
      checkLoops: false,
    }
  ],

  /**
   * This rule disallows continue statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-continue.md
   */
  "no-continue": [
    "off",
  ],

  /**
   * This rule disallows control characters in regular expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-control-regex.md
   */
  "no-control-regex": [
    "error",
  ],

  /**
   * This rule disallows debugger statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-debugger.md
   */
  "no-debugger": [
    "error",
  ],

  /**
   * This rule disallows the use of the delete operator on variables.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-delete-var.md
   */
  "no-delete-var": [
    "error",
  ],

  /**
   * This is used to disambiguate the division operator to not confuse users.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-div-regex.md
   */
  "no-div-regex": [
    "error",
  ],

  /**
   * This rule disallows duplicate parameter names in function declarations or
   * expressions. It does not apply to arrow functions or class methods,
   * because the parser reports the error.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-dupe-args.md
   */
  "no-dupe-args": [
    "error",
  ],

  /**
   * This rule is aimed to flag the use of duplicate names in class members.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-dupe-class-members.md
   */
  "no-dupe-class-members": [
    "error",
  ],

  /**
   * This rule disallows duplicate keys in object literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-dupe-keys.md
   */
  "no-dupe-keys": [
    "error",
  ],

  /**
   * This rule disallows duplicate test expressions in case clauses of switch statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-duplicate-case.md
   */
  "no-duplicate-case": [
    "error",
  ],

  /**
   * This rules requires that all imports from a single module exists in a single import statement.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-duplicate-imports.md
   */
  "no-duplicate-imports": [
    "off",
  ],

  /**
   * This rule is aimed at highlighting an unnecessary block of code following an
   * if containing a return statement. As such, it will warn when it encounters an
   * else following a chain of ifs, all of them containing a return statement.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-else-return.md
   */
  "no-else-return": [
    "error",
  ],

  /**
   * This rule disallows empty character classes in regular expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-empty-character-class.md
   */
  "no-empty-character-class": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating empty functions. A function will not be
   * considered a problem if it contains a comment.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-empty-function.md
   */
  "no-empty-function": [
    "off",
  ],

  /**
   * This rule aims to flag any empty patterns in destructured objects and arrays,
   * and as such, will report a problem whenever one is encountered.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-empty-pattern.md
   */
  "no-empty-pattern": [
    "error",
  ],

  /**
   * This rule disallows empty block statements. This rule ignores block statements
   * which contain a comment (for example, in an empty catch or finally block of
   * a try statement to indicate that execution should continue regardless of
   * errors).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-empty.md
   */
  "no-empty": [
    "error",
    {
      allowEmptyCatch: true,
    },
  ],

  /**
   * The no-eq-null rule aims reduce potential bug and unwanted behavior by
   * ensuring that comparisons to null only match null, and not also undefined.
   * As such it will flag comparisons to null when using == and !=.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-eq-null.md
   */
  "no-eq-null": [
    "error",
  ],

  /**
   * This rule is aimed at preventing potentially dangerous, unnecessary, and
   * slow code by disallowing the use of the eval() function. As such, it will
   * warn whenever the eval() function is used.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-eval.md
   */
  "no-eval": [
    "error",
  ],

  /**
   * This rule disallows reassigning exceptions in catch clauses.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-ex-assign.md
   */
  "no-ex-assign": [
    "error",
  ],

  /**
   * Disallows directly modifying the prototype of builtin objects.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-extend-native.md
   */
  "no-extend-native": [
    "error",
  ],

  /**
   * This rule is aimed at avoiding the unnecessary use of bind() and as such
   * will warn whenever an immediately-invoked function expression (IIFE) is
   * using bind() and doesn't have an appropriate this value. This rule won't
   * flag usage of bind() that includes function argument binding.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-extra-bind.md
   */
  "no-extra-bind": [
    "error",
  ],

  /**
   * This rule disallows unnecessary boolean casts. In contexts such as an if
   * statement's test where the result of the expression will already be coerced
   * to a Boolean, casting to a Boolean via double negation (!!) or a Boolean
   * call is unnecessary.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-extra-boolean-cast.md
   */
  "no-extra-boolean-cast": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating unnecessary labels. If a loop contains no
   * nested loops or switches, labeling the loop is unnecessary.
   *
   * ```js
   * A: while (a) {
   *  break A;
   * }
   * ```
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-extra-label.md
   */
  "no-extra-label": [
    "error",
  ],

  /**
   * This rule restricts the use of parentheses to only where they are necessary.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-extra-parens.md
   */
  "no-extra-parens": [
    "off", // Handled by @typescript-eslint/no-extra-parens
    "all",
    {
      conditionalAssign: true,
      nestedBinaryExpressions: false,
      returnAssign: false,
      ignoreJSX: "all",
      enforceForArrowConditionals: false,
    },
  ],

  /**
   * This rule disallows unnecessary semicolons.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-extra-semi.md
   */
  "no-extra-semi": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating unintentional fallthrough of one case to
   * the other. As such, it flags any fallthrough scenarios that are not marked
   * by a comment.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-fallthrough.md
   */
  "no-fallthrough": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating floating decimal points and will warn
   * whenever a numeric value has a decimal point but is missing a number either
   * before or after it.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-floating-decimal.md
   */
  "no-floating-decimal": [
    "error",
  ],

  /**
   * This rule disallows reassigning function declarations.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-func-assign.md
   */
  "no-func-assign": [
    "error",
  ],

  /**
   * This rule disallows modifications to read-only global variables.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-global-assign.md
   */
  "no-global-assign": [
    "error",
  ],

  /**
   * This rule is aimed to flag shorter notations for the type conversion, then
   * suggest a more self-explanatory notation.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-implicit-coercion.md
   */
  "no-implicit-coercion": [
    "error",
  ],

  /**
   * This rule disallows var and named function declarations at the top-level
   * script scope. This does not apply to ES and CommonJS modules since they
   * have a module scope.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-implicit-globals.md
   */
  "no-implicit-globals": [
    "error",
  ],

  /**
   * This rule aims to eliminate implied eval() through the use of setTimeout(),
   * setInterval() or execScript(). As such, it will warn when either function
   * is used with a string as the first argument.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-implied-eval.md
   */
  "no-implied-eval": [
    "error",
  ],

  /**
   * This rule disallows comments on the same line as code.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-inline-comments.md
   */
  "no-inline-comments": [
    "error",
  ],

  /**
   * This rule requires that function declarations and, optionally, variable
   * declarations be in the root of a program or the body of a function.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-inner-declarations.md
   */
  "no-inner-declarations": [
    "error",
  ],

  /**
   * This rule disallows invalid regular expression strings in RegExp
   * constructors.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-invalid-regexp.md
   */
  "no-invalid-regexp": [
    "error",
  ],

  /**
   * This rule aims to flag usage of this keywords outside of classes or
   * class-like objects.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-invalid-this.md
   */
  "no-invalid-this": [
    "error",
  ],

  /**
   * This rule is aimed at catching invalid whitespace that is not a normal tab
   * and space. Some of these characters may cause issues in modern browsers and
   * others will be a debugging issue to spot.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-irregular-whitespace.md
   */
  "no-irregular-whitespace": [
    "error",
  ],

  /**
   * This rule is aimed at preventing errors that may arise from using the
   * __iterator__ property, which is not implemented in several browsers.
   * As such, it will warn whenever it encounters the __iterator__ property.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-iterator.md
   */
  "no-iterator": [
    "error",
  ],

  /**
   * This rule aims to create clearer code by disallowing the bad practice of
   * creating a label that shares a name with a variable that is in scope.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-label-var.md
   */
  "no-label-var": [
    "error",
  ],

  /**
   * This rule aims to eliminate the use of labeled statements in JavaScript.
   * It will warn whenever a labeled statement is encountered and whenever
   * break or continue are used with a label.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-labels.md
   */
  "no-labels": [
    "error",
  ],

  /**
   * This rule aims to eliminate unnecessary and potentially confusing blocks
   * at the top level of a script or within other blocks.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-lone-blocks.md
   */
  "no-lone-blocks": [
    "error",
  ],

  /**
   * This rule disallows if statements as the only statement in else blocks.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-lonely-if.md
   */
  "no-lonely-if": [
    "error",
  ],

  /**
   * This rule disallows any function within a loop that contains unsafe
   * references (e.g. to modified variables from the outer scope).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-loop-func.md
   */
  "no-loop-func": [
    "error",
  ],

  /**
   * The no-magic-numbers rule aims to make code more readable and refactoring
   * easier by ensuring that special numbers are declared as constants to make
   * their meaning explicit.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-magic-numbers.md
   */
  "no-magic-numbers": [
    "off",
    {
      ignoreArrayIndexes: true,
      enforceConst: true,
      detectObjects: false,
      ignore: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    }
  ],

  /**
   * This rule reports the regular expressions which include multiple code point
   * characters in character class syntax. This rule considers the following
   * characters as multiple code point characters.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-misleading-character-class.md
   */
  "no-misleading-character-class": [
    "error",
  ],

  /**
   * This rule checks BinaryExpression and LogicalExpression.
   *
   * This rule may conflict with no-extra-parens rule. If you use both this and
   * no-extra-parens rule together, you need to use the nestedBinaryExpressions
   * option of no-extra-parens rule.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-mixed-operators.md
   */
  "no-mixed-operators": [
    "error",
    {
      groups: [
        ["%", "**"],
        ["%", "+"],
        ["%", "-"],
        ["%", "*"],
        ["%", "/"],
        ["**", "+"],
        ["**", "-"],
        ["**", "*"],
        ["**", "/"],
        ["&", "|", "^", "~", "<<", ">>", ">>>"],
        ["==", "!=", "===", "!==", ">", ">=", "<", "<="],
        ["&&", "||"],
        ["in", "instanceof"],
      ],
      allowSamePrecedence: false,
    },
  ],

  /**
   * When this rule is enabled, each var statement must satisfy the following conditions:
   *   - either none or all variable declarations must be require declarations (default)
   *   - all require declarations must be of the same type (grouping)
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-mixed-requires.md
   */
  "no-mixed-requires": [
    "error",
    {
      allowCall: false,
      grouping: true,
    },
  ],

  /**
   * This rule disallows mixed spaces and tabs for indentation.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-mixed-spaces-and-tabs.md
   */
  "no-mixed-spaces-and-tabs": [
    "error",
  ],

  /**
   * This rule disallows using multiple assignments within a single statement.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-multi-assign.md
   */
  "no-multi-assign": [
    "error",
  ],

  /**
   * This rule aims to disallow multiple whitespace around logical expressions,
   * conditional expressions, declarations, array elements, object properties,
   * sequences and function parameters.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-multi-spaces.md
   */
  "no-multi-spaces": [
    "error",
  ],

  /**
   * This rule is aimed at preventing the use of multiline strings.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-multi-str.md
   */
  "no-multi-str": [
    "error",
  ],

  /**
   * This rule aims to reduce the scrolling required when reading through your
   * code. It will warn when the maximum amount of empty lines has been exceeded.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-multiple-empty-lines.md
   */
  "no-multiple-empty-lines": [
    "error",
    {
      max: 2,
    },
  ],

  /**
   * This rule disallows negated conditions in either of the following:
   *   - if statements which have an else branch
   *   - ternary expressions
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-negated-condition.md
   */
  "no-negated-condition": [
    "error",
  ],

  /**
   * Just as developers might type -a + b when they mean -(a + b) for the negative
   * of a sum, they might type !key in object by mistake when they almost
   * certainly mean !(key in object) to test that a key is not in an object. This
   * rule disallows negating the left operand in in expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-negated-in-lhs.md
   */
  "no-negated-in-lhs": [
    "error",
  ],

  /**
   * The no-nested-ternary rule disallows nested ternary expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-nested-ternary.md
   */
  "no-nested-ternary": [
    "off",
  ],

  /**
   * This error is raised to highlight the use of a bad practice. By passing a
   * string to the Function constructor, you are requiring the engine to parse
   * that string much in the way it has to when you call the eval function.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-new-func.md
   */
  "no-new-func": [
    "error",
  ],

  /**
   * This rule disallows Object constructors.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-new-object.md
   */
  "no-new-object": [
    "error",
  ],

  /**
   * This rule aims to eliminate use of the new require expression.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-new-require.md
   */
  "no-new-require": [
    "error",
  ],

  /**
   * This rule is aimed at preventing the accidental calling of Symbol with the
   * new operator.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-new-symbol.md
   */
  "no-new-symbol": [
    "error",
  ],

  /**
   * This rule aims to eliminate the use of String, Number, and Boolean with
   * the new operator. As such, it warns whenever it sees new String,
   * new Number, or new Boolean.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-new-wrappers.md
   */
  "no-new-wrappers": [
    "error",
  ],

  /**
   * This rule is aimed at maintaining consistency and convention by disallowing
   * constructor calls using the new keyword that do not assign the resulting
   * object to a variable.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-new.md
   */
  "no-new": [
    "off",
  ],

  /**
   * This rule disallows calling the Math, JSON and Reflect objects as functions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-obj-calls.md
   */
  "no-obj-calls": [
    "error",
  ],

  /**
   * This rule disallows octal escape sequences in string literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-octal-escape.md
   */
  "no-octal-escape": [
    "error",
  ],

  /**
   * The rule disallows octal literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-octal.md
   */
  "no-octal": [
    "error",
  ],

  /**
   * This rule aims to prevent unintended behavior caused by modification or
   * reassignment of function parameters.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-param-reassign.md
   */
  "no-param-reassign": [
    "error",
  ],

  /**
   * This rule aims to prevent string concatenation of directory paths in Node.js
   * like __dirname and __filename.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-path-concat.md
   */
  "no-path-concat": [
    "error",
  ],

  /**
   * This rule disallows the unary operators ++ and --.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-plusplus.md
   */
  "no-plusplus": [
    "off",
  ],

  /**
   * This rule is aimed at discouraging use of process.env to avoid global
   * dependencies. As such, it will warn whenever process.env is used.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-process-env.md
   */
  "no-process-env": [
    "off",
  ],

  /**
   * This rule aims to prevent the use of process.exit() in Node.js JavaScript.
   * As such, it warns whenever process.exit() is found in code.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-process-exit.md
   */
  "no-process-exit": [
    "error",
  ],

  /**
   * When an object is created __proto__ is set to the original prototype
   * property of the object’s constructor function. getPrototypeOf is the
   * preferred method of getting "the prototype".
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-proto.md
   */
  "no-proto": [
    "error",
  ],

  /**
   * This rule disallows calling some Object.prototype methods directly on
   * object instances.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-prototype-builtins.md
   */
  "no-prototype-builtins": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating variables that have multiple declarations
   * in the same scope.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-redeclare.md
   */
  "no-redeclare": [
    "error",
    {
      builtinGlobals: true,
    }
  ],

  /**
   * This rule disallows multiple spaces in regular expression literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-regex-spaces.md
   */
  "no-regex-spaces": [
    "error",
  ],

  /**
   * This rule allows you to specify global variable names that you don't
   * want to use in your application.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-restricted-globals.md
   */
  "no-restricted-globals": [
    "off",
  ],

  /**
   * This rule allows you to specify imports that you don't want to use in your
   * application.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-restricted-imports.md
   */
  "no-restricted-imports": [
    "off",
  ],

  /**
   * This rule allows you to specify modules that you don’t want to use in your
   * application.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-restricted-modules.md
   */
  "no-restricted-modules": [
    "off",
  ],

  /**
   * This rule looks for accessing a given property key on a given object name,
   * either when reading the property's value or invoking it as a function.
   * You may specify an optional message to indicate an alternative API or a
   * reason for the restriction.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-restricted-properties.md
   */
  "no-restricted-properties": [
    "off",
  ],

  /**
   * This rule disallows specified (that is, user-defined) syntax.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-restricted-syntax.md
   */
  "no-restricted-syntax": [
    "off",
  ],

  /**
   * This rule aims to eliminate assignments from return statements. As such,
   * it will warn whenever an assignment is found as part of return.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-return-assign.md
   */
  "no-return-assign": [
    "error",
  ],

  /**
   * This rule aims to prevent a likely common performance hazard due to a lack
   * of understanding of the semantics of async function.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-return-await.md
   */
  "no-return-await": [
    "error",
  ],

  /**
   * Using javascript: URLs is considered by some as a form of eval. Code passed
   * in javascript: URLs has to be parsed and evaluated by the browser in the
   * same way that eval is processed.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-script-url.md
   */
  "no-script-url": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating self assignments.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-self-assign.md
   */
  "no-self-assign": [
    "error",
  ],

  /**
   * This error is raised to highlight a potentially confusing and potentially
   * pointless piece of code. There are almost no situations in which you would
   * need to compare something to itself.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-self-compare.md
   */
  "no-self-compare": [
    "error",
  ],

  /**
   * This rule forbids the use of the comma operator, with the following exceptions:
   *   - In the initialization or update portions of a for statement.
   *   - If the expression sequence is explicitly wrapped in parentheses.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-sequences.md
   */
  "no-sequences": [
    "error",
  ],

  /**
   * ES5 §15.1.1 Value Properties of the Global Object (NaN, Infinity, undefined)
   * as well as strict mode restricted identifiers eval and arguments are
   * considered to be restricted names in JavaScript. Defining them to mean
   * something else can have unintended consequences and confuse others reading
   * the code. For example, there's nothing prevent you from writing:
   *
   * ```js
   * var undefined = "foo";
   * ```
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-shadow-restricted-names.md
   */
  "no-shadow-restricted-names": [
    "error",
  ],

  /**
   * This rule aims to eliminate shadowed variable declarations.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-shadow.md
   */
  "no-shadow": [
    "error",
    {
      builtinGlobals: true,
      hoist: "functions",
    }
  ],

  /**
   * This rule is aimed at preventing synchronous methods from being called
   * in Node.js. It looks specifically for the method suffix "Sync"
   * (as is the convention with Node.js operations).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-sync.md
   */
  "no-sync": [
    "error",
  ],

  /**
   * This rule disallows sparse array literals which have "holes" where commas
   * are not preceded by elements. It does not apply to a trailing comma
   * following the last element
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-sparse-arrays.md
   */
  "no-sparse-arrays": [
    "error",
  ],

  /**
   * This rule looks for tabs anywhere inside a file: code, comments or
   * anything else.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-tabs.md
   */
  "no-tabs": [
    "error",
  ],

  /**
   * This rule aims to warn when a regular string contains what looks like a
   * template literal placeholder. It will warn when it finds a string containing
   * the template literal place holder (${something}) that uses either " or '
   * for the quotes.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-template-curly-in-string.md
   */
  "no-template-curly-in-string": [
    "error",
  ],

  /**
   * This rule disallows ternary operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-ternary.md
   */
  "no-ternary": [
    "off",
  ],

  /**
   * This rule is aimed to flag this/super keywords before super() callings.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-this-before-super.md
   */
  "no-this-before-super": [
    "error",
  ],

  /**
   * This rule is aimed at maintaining consistency when throwing exception by
   * disallowing to throw literals and other expressions which cannot possibly
   * be an Error object.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-throw-literal.md
   */
  "no-throw-literal": [
    "error",
  ],

  /**
   * This rule disallows trailing whitespace (spaces, tabs, and other Unicode
   * whitespace characters) at the end of lines.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-trailing-spaces.md
   */
  "no-trailing-spaces": [
    "error",
  ],

  /**
   * This rule aims to eliminate variable declarations that initialize to undefined.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-undef-init.md
   */
  "no-undef-init": [
    "error",
  ],

  /**
   * Any reference to an undeclared variable causes a warning, unless the variable
   * is explicitly mentioned in a global comment, or specified in the
   * globals key in the configuration file. A common use case for these is if
   * you intentionally use globals that are defined elsewhere (e.g. in a script
   * sourced from HTML).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-undef.md
   */
  "no-undef": [
    "off", // Going to be handled by tslint
  ],

  /**
   * This rule aims to eliminate the use of undefined, and as such, generates a
   * warning whenever it is used.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-undefined.md
   */
  "no-undefined": [
    "error",
  ],

  /**
   * This rule disallows dangling underscores in identifiers.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-underscore-dangle.md
   */
  "no-underscore-dangle": [
    "off",
  ],

  /**
   * This rule disallows confusing multiline expressions where a newline looks
   * like it is ending a statement, but is not.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unexpected-multiline.md
   */
  "no-unexpected-multiline": [
    "error",
  ],

  /**
   * This rule finds references which are inside of loop conditions, then checks
   * the variables of those references are modified in the loop.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unmodified-loop-condition.md
   */
  "no-unmodified-loop-condition": [
    "error",
  ],

  /**
   * This rule disallow ternary operators when simpler alternatives exist.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unneeded-ternary.md
   */
  "no-unneeded-ternary": [
    "error",
  ],

  /**
   * This rule disallows unreachable code after return, throw, continue, and
   * break statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unreachable.md
   */
  "no-unreachable": [
    "error",
  ],

  /**
   * This rule disallows return, throw, break, and continue statements inside
   * finally blocks. It allows indirect usages, such as in function or class
   * definitions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unsafe-finally.md
   */
  "no-unsafe-finally": [
    "error",
  ],

  /**
   * This rule disallows negating the left operand of Relational Operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unsafe-negation.md
   */
  "no-unsafe-negation": [
    "error",
  ],

  /**
   * This rule aims to eliminate unused expressions which have no effect on the
   * state of the program.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unused-expressions.md
   */
  "no-unused-expressions": [
    "off",
  ],

  /**
   * This rule is aimed at eliminating unused labels.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unused-labels.md
   */
  "no-unused-labels": [
    "error",
  ],

  /**
   * This rule is aimed at eliminating unused variables, functions, and parameters
   * of functions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-unused-vars.md
   */
  "no-unused-vars": [
    "off" // Going to be handled by @typescript-eslint/no-unused-vars
  ],

  /**
   * This rule will warn when it encounters a reference to an identifier that
   * has not yet been declared.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-use-before-define.md
   */
  "no-use-before-define": [
    "error",
    {
      functions: true,
      classes: true,
    },
  ],

  /**
   * This rule is aimed to flag usage of Function.prototype.call() and
   * Function.prototype.apply() that can be replaced with the normal function
   * invocation.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-call.md
   */
  "no-useless-call": [
    "error",
  ],

  /**
   * This rule disallows unnecessary usage of computed property keys.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-computed-key.md
   */
  "no-useless-computed-key": [
    "error",
  ],

  /**
   * This rule aims to flag the concatenation of 2 literals when they could be
   * combined into a single literal. Literals can be strings or template literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-concat.md
   */
  "no-useless-concat": [
    "error",
  ],

  /**
   * This rule flags class constructors that can be safely removed without
   * changing how the class works.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-constructor.md
   */
  "no-useless-constructor": [
    "off", // Override by @typescript-eslint/no-useless-constructor
  ],

  /**
   * This rule flags escapes that can be safely removed without changing behavior.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-escape.md
   */
  "no-useless-escape": [
    "error",
  ],

  /**
   * This rule disallows the renaming of import, export, and destructured
   * assignments to the same name.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-rename.md
   */
  "no-useless-rename": [
    "error",
  ],

  /**
   * This rule aims to report redundant return statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-useless-return.md
   */
  "no-useless-return": [
    "error",
  ],

  /**
   * This rule is aimed at discouraging the use of var and encouraging the use
   * of const or let instead.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-var.md
   */
  "no-var": [
    "error",
  ],

  /**
   * This rule aims to eliminate use of void operator.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-void.md
   */
  "no-void": [
    "error",
  ],

  /**
   * This rule reports comments that include any of the predefined terms
   * specified in its configuration.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-warning-comments.md
   */
  "no-warning-comments": [
    "off",
  ],

  /**
   * This rule disallows whitespace around the dot or before the opening bracket
   * before properties of objects if they are on the same line.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-whitespace-before-property.md
   */
  "no-whitespace-before-property": [
    "error",
  ],

  /**
   * This rule disallows with statements.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/no-with.md
   */
  "no-with": [
    "error",
  ],

  /**
   * This rule aims to enforce a consistent location for single-line statements.
   * Note that this rule does not enforce the usage of single-line statements in
   * general. If you would like to disallow single-line statements, use the curly
   * rule instead.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/nonblock-statement-body-position.md
   */
  "nonblock-statement-body-position": [
    "off",
  ],

  /**
   * This rule enforces consistent line breaks inside braces of object literals
   * or destructuring assignments.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/object-curly-newline.md
   */
  "object-curly-newline": [
    "error",
    {
      minProperties: 3,
      consistent: true,
    },
  ],

  /**
   * This rule enforce consistent spacing inside braces of object literals,
   * destructuring assignments, and import/export specifiers.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/object-curly-spacing.md
   */
  "object-curly-spacing": [
    "error",
    "always",
    {
      arraysInObjects: true,
      objectsInObjects: true,
    },
  ],

  /**
   * This rule makes it possible to ensure, as some style guides require, that
   * property specifications appear on separate lines for better readability.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/object-property-newline.md
   */
  "object-property-newline": [
    "error",
    {
      allowMultiplePropertiesPerLine: true,
    }
  ],

  /**
   * This rule enforces the use of the shorthand syntax. This applies to all
   * methods (including generators) defined in object literals and any properties
   * defined where the key name matches name of the assigned variable.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/object-shorthand.md
   */
  "object-shorthand": [
    "error",
    "always",
  ],

  /**
   * This rule enforces a consistent newlines around variable declarations.
   * This rule ignores variable declarations inside for loop conditionals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/one-var-declaration-per-line.md
   */
  "one-var-declaration-per-line": [
    "error",
  ],

  /**
   * This rule enforces variables to be declared either together or separately
   * per function (for var) or block (for let and const) scope.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/one-var.md
   */
  "one-var": [
    "error",
    "never",
  ],

  /**
   * This rule requires or disallows assignment operator shorthand where possible.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/operator-assignment.md
   */
  "operator-assignment": [
    "error",
    "always",
  ],

  /**
   * This rule enforces a consistent linebreak style for operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/operator-linebreak.md
   */
  "operator-linebreak": [
    "error",
    "before",
  ],

  /**
   * This rule enforces consistent empty line padding within blocks.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/padded-blocks.md
   */
  "padded-blocks": [
    "off",
  ],

  /**
   * This rule does nothing if no configurations are provided.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/padding-line-between-statements.md
   */
  "padding-line-between-statements": [
    "off",
  ],

  /**
   * This rule locates function expressions used as callbacks or function
   * arguments. An error will be produced for any that could be replaced by an
   * arrow function without changing the result.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-arrow-callback.md
   */
  "prefer-arrow-callback": [
    "error",
    {
      allowNamedFunctions: false,
      allowUnboundThis: true,
    },
  ],

  /**
   * This rule is aimed at flagging variables that are declared using let keyword,
   * but never reassigned after the initial assignment.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-const.md
   */
  "prefer-const": [
    "error",
  ],

  /**
   * With JavaScript ES6, a new syntax was added for creating variables from an
   * array index or object property, called destructuring. This rule enforces
   * usage of destructuring instead of accessing a property through a member
   * expression.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-destructuring.md
   */
  "prefer-destructuring": [
    "error",
    {
      VariableDeclarator: {
        array: true,
        object: true,
      },
      AssignmentExpression: {
        array: false,
        object: false,
      }
    },
    {
      enforceForRenamedProperties: false,
    },
  ],

  /**
   * This rule disallows calls to parseInt() or Number.parseInt() if called with
   * two arguments: a string; and a radix option of 2 (binary), 8 (octal), or
   * 16 (hexadecimal).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-numeric-literals.md
   */
  "prefer-numeric-literals": [
    "off",
  ],

  /**
   * When Object.assign is called using an object literal as the first argument,
   * this rule requires using the object spread syntax instead. This rule also
   * warns on cases where an Object.assign call is made using a single argument
   * that is an object literal, in this case, the Object.assign call is not
   * needed.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-object-spread.md
   */
  "prefer-object-spread": [
    "error",
  ],

  /**
   * This rule is aimed to flag usage of arguments variables.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-rest-params.md
   */
  "prefer-promise-reject-errors": [
    "error",
  ],

  /**
   * This rule is aimed to flag usage of Function.prototype.apply() in situations
   * where the spread operator could be used instead.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-spread.md
   */
  "prefer-spread": [
    "error",
  ],

  /**
   * This rule is aimed to flag usage of + operators with strings.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/prefer-template.md
   */
  "prefer-template": [
    "error",
  ],

  /**
   * This rule requires quotes around object literal property names.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/quote-props.md
   */
  "quote-props": [
    "error",
    "as-needed",
  ],

  /**
   * This rule enforces the consistent use of either backticks, double, or
   * single quotes.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/quotes.md
   */
  "quotes": [
    "off", // Overriden by @typescript-eslint/quotes
    "double",
    {
      allowTemplateLiterals: true,
    },
  ],

  /**
   * This rule is aimed at preventing the unintended conversion of a string to a
   * number of a different base than intended or at preventing the redundant 10
   * radix if targeting modern environments only.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/radix.md
   */
  "radix": [
    "error",
    "always",
  ],

  /**
   * This rule aims to report assignments to variables or properties where all
   * of the following are true:
   *   - A variable or property is reassigned to a new value which is based on its
   *   old value.
   *   - A yield or await expression interrupts the assignment after the old
   *    value is read, and before the new value is set.
   *   - The rule cannot easily verify that the assignment is safe (e.g. if an
   *   assigned variable is local and would not be readable from anywhere else
   *   while the function is paused).
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/require-atomic-updates.md
   */
  "require-atomic-updates": [
    "error",
  ],

  /**
   * This rule warns async functions which have no await expression.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/require-await.md
   */
  "require-await": [
    "off", // Handled by @typescript-eslint/require-await
  ],

  /**
   * This rule requires JSDoc comments for specified nodes.
   *
   * @deprecated
   *
   * https://eslint.org/docs/rules/require-jsdoc
   */
  "require-jsdoc": [
    "error",
    {
      require: {
        FunctionDeclaration: true,
        MethodDefinition: true,
        ClassDeclaration: true,
        ArrowFunctionExpression: true,
        FunctionExpression: true,
      },
    },
  ],

  /**
   * This rule aims to enforce the use of u flag on regular expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/require-unicode-regexp.md
   */
  "require-unicode-regexp": [
    "off",
  ],

  /**
   * This rule generates warnings for generator functions that do not have the
   * yield keyword.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/require-yield.md
   */
  "require-yield": [
    "error",
  ],

  /**
   * This rule aims to enforce consistent spacing between rest and spread
   * operators and their expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/rest-spread-spacing.md
   */
  "rest-spread-spacing": [
    "error",
    "never",
  ],

  /**
   * This rule aims to enforce spacing around a semicolon. This rule prevents
   * the use of spaces before a semicolon in expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/semi-spacing.md
   */
  "semi-spacing": [
    "error",
    {
      before: false,
      after: true,
    }
  ],

  /**
   * This rule reports line terminators around semicolons.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/semi-style.md
   */
  "semi-style": [
    "error",
    "last",
  ],

  /**
   * This rule enforces consistent use of semicolons.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/semi.md
   */
  "semi": [
    "off", // Handled by @typescript-eslint/semi
    "always",
  ],

  /**
   * This rule checks all import declarations and verifies that all imports are
   * first sorted by the used member syntax and then alphabetically by the first
   * member or alias name.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/sort-imports.md
   */
  "sort-imports": [
    "off"
  ],

  /**
   * This rule checks all property definitions of object expressions and verifies
   * that all variables are sorted alphabetically.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/sort-keys.md
   */
  "sort-keys": [
    "error",
  ],

  /**
   * This rule checks all variable declaration blocks and verifies that all
   * variables are sorted alphabetically. The default configuration of the rule
   * is case-sensitive.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/sort-vars.md
   */
  "sort-vars": [
    "error",
  ],

  /**
   * This rule will enforce consistency of spacing before blocks. It is only
   * applied on blocks that don’t begin on a new line.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/space-before-blocks.md
   */
  "space-before-blocks": [
    "error",
    "always",
  ],

  /**
   * This rule aims to enforce consistent spacing before function parentheses
   * and as such, will warn whenever whitespace doesn't match the preferences
   * specified.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/space-before-function-paren.md
   */
  "space-before-function-paren": [
    "off", // Handled by @typescript-eslint/space-before-function-paren
    {
      anonymous: "never",
      named: "never",
      asyncArrow: "always",
    },
  ],

  /**
   * This rule will enforce consistency of spacing directly inside of parentheses,
   * by disallowing or requiring one or more spaces to the right of ( and to the
   * left of ). In either case, () will still be allowed.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/space-in-parens.md
   */
  "space-in-parens": [
    "error",
    "never",
  ],

  /**
   * This rule is aimed at ensuring there are spaces around infix operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/space-infix-ops.md
   */
  "space-infix-ops": [
    "error",
  ],

  /**
   * This rule enforces consistency regarding the spaces after words unary
   * operators and after/before nonwords unary operators.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/space-unary-ops.md
   */
  "space-unary-ops": [
    "error",
  ],

  /**
   * This rule will enforce consistency of spacing after the start of a comment
   * // or /*. It also provides several exceptions for various documentation
   * styles.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/spaced-comment.md
   */
  "spaced-comment": [
    "error",
    "always",
    {
      exceptions: ["#"]
    }
  ],

  /**
   * This rule requires or disallows strict mode directives.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/strict.md
   */
  "strict": [
    "off",
  ],

  /**
   * This rule controls spacing around colons of case and default clauses in
   * switch statements. This rule does the check only if the consecutive tokens
   * exist on the same line.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/switch-colon-spacing.md
   */
  "switch-colon-spacing": [
    "error",
    {
      before: false,
      after: true,
    },
  ],

  /**
   * This rules requires a description when creating symbols.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/symbol-description.md
   */
  "symbol-description": [
    "error",
  ],

  /**
   * This rule aims to maintain consistency around the spacing inside of template
   * literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/template-curly-spacing.md
   */
  "template-curly-spacing": [
    "error",
    "never",
  ],

  /**
   * This rule aims to maintain consistency around the spacing between template
   * tag functions and their template literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/template-tag-spacing.md
   */
  "template-tag-spacing": [
    "error",
    "never",
  ],

  /**
   * If the "always" option is used, this rule requires that files always begin
   * with the Unicode BOM character U+FEFF. If "never" is used, files must never
   * begin with U+FEFF.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/unicode-bom.md
   */
  "unicode-bom": [
    "error",
    "never",
  ],

  /**
   * This rule disallows comparisons to 'NaN'.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/use-isnan.md
   */
  "use-isnan": [
    "error",
  ],

  /**
   * This rule enforces comparing typeof expressions to valid string literals.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/valid-typeof.md
   */
  "valid-typeof": [
    "error",
  ],

  /**
   * This rule aims to keep all variable declarations in the leading series of
   * statements. Allowing multiple declarations helps promote maintainability
   * and is thus allowed.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/vars-on-top.md
   */
  "vars-on-top": [
    "off",
  ],

  /**
   * This rule requires all immediately-invoked function expressions to be
   * wrapped in parentheses.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/wrap-iife.md
   */
  "wrap-iife": [
    "error",
    "inside",
  ],

  /**
   * This is used to disambiguate the slash operator and facilitates more
   * readable code.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/wrap-regex.md
   */
  "wrap-regex": [
    "error",
  ],

  /**
   * This rule enforces spacing around the * in yield* expressions.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/yield-star-spacing.md
   */
  "yield-star-spacing": [
    "error",
  ],

  /**
   * This rule aims to enforce consistent style of conditions which compare a
   * variable to a literal value.
   *
   * https://github.com/eslint/eslint/blob/master/docs/rules/yoda.md
   */
  "yoda": [
    "error",
    "never",
  ],
});

//#####################################################
// Import Rules
//#####################################################
const importRules = (opts) => ({
  /**
   * If a default import is requested, this rule will report if there is no default
   * export in the imported module.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/default.md
   */
  "import/default": [
    "off", // handled by tslint
  ],

  /**
   * This rule enforces naming of webpack chunks in dynamic imports.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/dynamic-import-chunkname.md
   */
  "import/dynamic-import-chunkname": [
    "error",
  ],

  /**
   * Reports funny business with exports, like repeated exports of names or defaults.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/export.md
   */
  "import/export": [
    "error",
  ],

  /**
   * This rule can enforce or disallow the use of certain file extensions.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/extensions.md
   */
  "import/extensions": [
    "off",
    "ignorePackages",
    {
      js: "never",
      jsx: "never",
      ts: "never",
      tsx: "never",
    }
  ],

  /**
   * This rule enforces that all exports are declared at the bottom of the file.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/exports-last.md
   */
  "import/exports-last": [
    "off"
  ],

  /**
   * This rule reports any imports that come after non-import statements.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/first.md
   */
  "import/first": [
    "error",
  ],

  /**
   * Reports when named exports are not grouped together in a single export
   * declaration or when multiple assignments to CommonJS module.exports or
   * exports object are present in a single file.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/group-exports.md
   */
  "import/group-exports": [
    "off"
  ],

  /**
   * Forbid modules to have too many dependencies (import or require statements).
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/max-dependencies.md
   */
  "import/max-dependencies": [
    "off",
  ],

  /**
   * Verifies that all named imports are part of the set of named exports in
   * the referenced module.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/named.md
   */
  "import/named": [
    "off", // Going to be handled by tslint
  ],

  /**
   * Enforces names exist at the time they are dereferenced, when imported as
   * a full namespace (i.e. import * as foo from './foo'; foo.bar(); will report
   * if bar is not exported by ./foo.).
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/namespace.md
   */
  "import/namespace": [
    "error",
  ],

  /**
   * Enforces having one or more empty lines after the last top-level import
   * statement or require call
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/newline-after-import.md
   */
  "import/newline-after-import": [
    "error",
  ],

  /**
   * Node.js allows the import of modules using an absolute path such as /home/xyz/file.js.
   * That is a bad practice as it ties the code using it to your computer, and therefore makes
   * it unusable in packages distributed on npm for instance.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-absolute-path.md
   */
  "import/no-absolute-path": [
    "error",
  ],

  /**
   * Reports require([array], ...) and define([array], ...) function calls at
   * the module scope. Will not report if !=2 arguments, or first argument is
   * not a literal array.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-amd.md
   */
  "import/no-amd": [
    "error",
  ],

  /**
   * Reports if a module's default export is unnamed.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-anonymous-default-export.md
   */
  "import/no-anonymous-default-export": [
    "error",
  ],

  /**
   * Reports require([string]) function calls. Will not report if >1 argument,
   * or single argument is not a literal string. Reports module.exports or
   * exports.*, also.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-commonjs.md
   */
  "import/no-commonjs": [
    "error",
  ],

  /**
   * Ensures that there is no resolvable path back to this module via its dependencies.
   * This rule is comparatively computationally expensive.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-cycle.md
   */
  "import/no-cycle": [
    "off",
  ],

  /**
   * Prohibit default exports.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-default-export.md
   */
  "import/no-default-export": [
    "off",
  ],

  /**
   * Reports use of a deprecated name, as indicated by a JSDoc block with a
   * @deprecated tag or TomDoc Deprecated: comment.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-deprecated.md
   */
  "import/no-deprecated": [
    "error"
  ],

  /**
   * Reports if a resolved path is imported more than once.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-duplicates.md
   */
  "import/no-duplicates": [
    "off", // Handled by tslint
  ],

  /**
   * The require method from CommonJS can be given expressions that will be
   * resolved at runtime.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-dynamic-require.md
   */
  "import/no-dynamic-require": [
    "error",
  ],

  /**
   * Forbid the import of external modules that are not declared in the package.json's
   * dependencies, devDependencies, optionalDependencies or peerDependencies
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-extraneous-dependencies.md
   */
  "import/no-extraneous-dependencies": [
    "error",
    {
      packageDir: opts.platform === "REACT_NATIVE"
        ? [opts.packageDir || __dirname]
        : []
    }
  ],

  /**
   * Use this rule to prevent importing the submodules of other modules.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-internal-modules.md
   */
  "import/no-internal-modules": [
    "off",
  ],

  /**
   * Forbids the use of mutable exports with var or let.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-mutable-exports.md
   */
  "import/no-mutable-exports": [
    "error",
  ],

  /**
   * Reports use of an exported name as the locally imported name of a default
   * export.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-named-as-default.md
   */
  "import/no-named-as-default": [
    "off",
  ],

  /**
   * Reports if namespace import like `import * as foo from 'foo';` is used.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-namespace.md
   */
  "import/no-namespace": [
    "off",
  ],

  /**
   * Forbid the use of Node.js builtin modules. Can be useful for client-side
   * web projects that do not have access to those modules.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-nodejs-modules.md
   */
  "import/no-nodejs-modules": [
    "off",
  ],

  /**
   * Use this rule to prevent imports to folders in relative parent paths.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-relative-parent-imports.md
   */
  "import/no-relative-parent-imports": [
    "off",
  ],

  /**
   * Some projects contain files which are not always meant to be executed in the same environment.
   * For example consider a web application that contains specific code for the server and some
   * specific code for the browser/client. In this case you don’t want to import server-only
   * files in your client code.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-restricted-paths.md
   */
  "import/no-restricted-paths": [
    "off",
  ],

  /**
   * Forbid a module from importing itself. This can sometimes happen during
   * refactoring.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-self-import.md
   */
  "import/no-self-import": [
    "error",
  ],

  /**
   * With both CommonJS' require and the ES6 modules' import syntax, it is possible to import a
   * module but not to use its result. This can be done explicitly by not assigning the module
   * to as variable. This rule aims to remove modules with side-effects by reporting when a
   * module is imported but not assigned.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unassigned-import.md
   */
  "import/no-unassigned-import": [
    "off",
  ],

  /**
   * Ensure imports point to files/modules that can be resolved.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unresolved.md
   */
  "import/no-unresolved": [
    "error",
    {
      commonjs: true,
      caseSensitive: true,
    },
  ],

  /**
   * Use this rule to prevent unnecessary path segments in import and require statements.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-useless-path-segments.md
   */
  "import/no-useless-path-segments": [
    "error",
  ],

  /**
   * Forbid Webpack loader syntax in imports.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-webpack-loader-syntax.md
   */
  "import/no-webpack-loader-syntax": [
    "error",
  ],

  /**
   * Enforce a convention in the order of require() / import statements
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/order.md
   */
  "import/order": [
    "error",
    {
      groups: [
        "builtin",
        "external",
        "internal",
        "parent",
        "sibling",
        "index",
      ],
      "newlines-between": "always",
    }
  ],

  /**
   * When there is only a single export from a module, prefer using default export over named export.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/prefer-default-export.md
   */
  "import/prefer-default-export": [
    "error",
  ],

  /**
   * Warn if a module could be mistakenly parsed as a script by a consumer leveraging
   * Unambiguous JavaScript Grammar to determine correct parsing goal.
   *
   * https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/unambiguous.md
   */
  "import/unambiguous": [
    "off",
  ],
});

//#####################################################
// JSDoc Rules
//#####################################################
const jsdocRules = (opts) => ({
  /**
   * Ensures that (JavaScript) examples within JSDoc adhere to ESLint rules.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*check-examples
   */
  "jsdoc/check-examples": [
    "error",
  ],

  /**
   * Reports invalid block tag names.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*check-tag-names
   */
  "jsdoc/check-tag-names": [
    "error",
  ],

  /**
   * Reports invalid types.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*check-types
   */
  "jsdoc/check-types": [
    "off",
  ],

  /**
   * Enforces a consistent padding of the block description.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*newline-after-description
   */
  "jsdoc/newline-after-description": [
    "error",
  ],

  /**
   * Checks that types in jsdoc comments are defined. This can be used to check
   * unimported types.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*no-undefined-types
   */
  "jsdoc/no-undefined-types": [
    "error",
  ],

  /**
   * Requires that all functions have a description.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-description
   */
  "jsdoc/require-description": [
    "off",
  ],

  /**
   * Requires that block description and tag description are written in complete
   * sentences, i.e.,
   *   - Description must start with an uppercase alphabetical character.
   *   - Paragraphs must start with an uppercase alphabetical character.
   *   - Sentences must end with a period.
   *   - Every line in a paragraph (except the first) which starts with an
   *   uppercase character must be preceded by a line ending with a period.
   *
   *   https://github.com/gajus/eslint-plugin-jsdoc*require-description-complete-sentence
   */
  "jsdoc/require-description-complete-sentence": [
    "off",
  ],

  /**
   * Requires that all functions have examples.
   *   - All functions must have one or more @example tags.
   *   - Every example tag must have a non-empty description that explains the
   *   method's usage.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-example
   */
  "jsdoc/require-example": [
    "off",
  ],

  /**
   * Requires a hyphen before the @param description.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-hyphen-before-param-description
   */
  "jsdoc/require-hyphen-before-param-description": [
    "error",
  ],

  /**
   * Requires that all function parameters are documented.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-param
   */
  "jsdoc/require-param": [
    "error",
  ],

  /**
   * Requires that @param tag has description value.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-param-description
   */
  "jsdoc/require-param-description": [
    "error",
  ],

  /**
   * Requires that all function parameters have name.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-param-names
   */
  "jsdoc/require-param-name": [
    "error",
  ],

  /**
   * Requires that @param tag has type value.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-param-type
   */
  "jsdoc/require-param-type": [
    "off",
  ],

  /**
   * Requires that @returns tag has description value.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-returns-description
   */
  "jsdoc/require-returns-description": [
    "off",
  ],

  /**
   * Requires that @returns tag has type value.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*require-returns-type
   */
  "jsdoc/require-returns-type": [
    "error",
  ],

  /**
   * Requires all types to be valid JSDoc or Closure compiler types without
   * syntax errors.
   *
   * https://github.com/gajus/eslint-plugin-jsdoc*valid-types
   */
  "jsdoc/valid-types": [
    "error",
  ],
});

//#####################################################
// React Rules
//#####################################################
const reactRules = (opts) => ({
  /**
   * Allows you to enforce a consistent naming pattern for props which expect
   * a boolean value. Names should be like isEnabled, isAFK, hasCondition, hasLOL.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/boolean-prop-naming.md
   */
  "react/boolean-prop-naming": [
    "error",
    {
      rule: "^(is|has)[A-Z]([A-Za-z0-9]?)+"
    }
  ],

  /**
   * The default value of type attribute for button HTML element is "submit"
   * which is often not the desired behavior and may lead to unexpected page
   * reloads. This rules enforces an explicit type attribute for all the button
   * elements and checks that its value is valid per spec (i.e., is one of
   * "button", "submit", and "reset").
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/button-has-type.md
   */
  "react/button-has-type": [
    "error",
  ],

  /**
   * This rule aims to ensure that any defaultProp has a non-required PropType
   * declaration.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/default-props-match-prop-types.md
   */
  "react/default-props-match-prop-types": [
    "error",
  ],

  /**
   * Enforce consistent usage of destructuring assignment of props, state, and
   * context.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/destructuring-assignment.md
   */
  "react/destructuring-assignment": [
    "off",
  ],

  /**
   * DisplayName allows you to name your component. This name is used by React
   * in debugging messages.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/display-name.md
   */
  "react/display-name": [
    "off",
  ],

  /**
   * By default this rule prevents passing of props that add lots of complexity
   * (className, style) to Components.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/forbid-component-props.md
   */
  "react/forbid-component-props": [
    "off",
  ],

  /**
   * This rule prevents passing of props to elements. This rule only applies to
   * DOM Nodes (e.g. <div />) and not Components (e.g. <Component />).
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/forbid-dom-props.md
   */
  "react/forbid-dom-props": [
    "off",
  ],

  /**
   * You may want to forbid usage of certain elements in favor of others, (e.g.
   * forbid all <div /> and use <Box /> instead). This rule allows you to
   * configure a list of forbidden elements and to specify their desired
   * replacements.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/forbid-elements.md
   */
  "react/forbid-elements": [
    "off",
  ],

  /**
   * This rule forbids using another component's prop types unless they are
   * explicitly imported/exported. This allows people who want to use
   * babel-plugin-transform-react-remove-prop-types to remove propTypes from
   * their components in production builds, to do so safely.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/forbid-foreign-prop-types.md
   */
  "react/forbid-foreign-prop-types": [
    "error",
  ],

  /**
   * By default this rule prevents vague prop types with more specific alternatives
   * available (any, array, object), but any prop type can be disabled if desired.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/forbid-prop-types.md
   */
  "react/forbid-prop-types": [
    "off",
  ],

  /**
   * When using a boolean attribute in JSX, you can set the attribute value to
   * true or omit the value. This rule will enforce one or the other to keep
   * consistency in your code.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-boolean-value.md
   */
  "react/jsx-boolean-value": [
    "error",
    "always",
  ],

  /**
   * Since React removes extraneous new lines between elements when possible,
   * it is possible to end up with inline elements that are not rendered with
   * spaces between them and adjacent text.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-child-element-spacing.md
   */
  "react/jsx-child-element-spacing": [
    "error",
  ],

  /**
   * Enforce the closing bracket location for JSX multiline elements.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md
   */
  "react/jsx-closing-bracket-location": [
    "error",
    "tag-aligned",
  ],

  /**
   * Enforce the closing tag location for multiline JSX elements.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-tag-location.md
   */
  "react/jsx-closing-tag-location": [
    "error",
  ],

  /**
   * This rule allows you to enforce curly braces or disallow unnecessary curly
   * braces in JSX props and/or children.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-curly-brace-presence.md
   */
  "react/jsx-curly-brace-presence": [
    "off",
    {
      props: "never",
      children: "always",
    }
  ],

  /**
   * This rule aims to maintain consistency around the spacing inside of JSX
   * attributes and expressions inside element children.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-curly-spacing.md
   */
  "react/jsx-curly-spacing": [
    "error",
    {
      when: "never",
      children: true,
    }
  ],

  /**
   * This rule will enforce consistency of spacing around equal signs in JSX
   * attributes, by requiring or disallowing one or more spaces before and
   * after =.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-equals-spacing.md
   */
  "react/jsx-equals-spacing": [
    "error",
    "never",
  ],

  /**
   * Restrict file extensions that may contain JSX.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-filename-extension.md
   */
  "react/jsx-filename-extension": [
    "error",
    {
      extensions: [".jsx", ".tsx"],
    },
  ],

  /**
   * This rule checks whether the first property of all JSX elements is
   * correctly placed.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-first-prop-new-line.md
   */
  "react/jsx-first-prop-new-line": [
    "error",
    "multiline",
  ],

  /**
   * In JSX, a React fragment is created either with <React.Fragment>...</React.Fragment>,
   * or, using the shorthand syntax, <>...</>. This rule allows you to enforce
   * one way or the other.
   *
   * @WARNING : Still not available https://github.com/yannickcr/eslint-plugin-react/issues/2072
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-fragments.md
   */
  /*"react/jsx-fragments": [
    "error",
    "element",
  ],*/

  /**
   * Ensures that any component or prop methods used to handle events are
   * correctly prefixed.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-handler-names.md
   */
  "react/jsx-handler-names": [
    "error",
    {
      eventHandlerPrefix: "handle|_handle",
      eventHandlerPropPrefix: "on",
    },
  ],

  /**
   * This rule is aimed to enforce consistent indentation style.
   * The default style is 4 spaces.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-indent-props.md
   */
  "react/jsx-indent-props": [
    "error",
    2,
  ],

  /**
   * This rule is aimed to enforce consistent indentation style.
   * The default style is 4 spaces.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-indent.md
   */
  "react/jsx-indent": [
    "error",
    2,
  ],

  /**
   * Warn if an element that likely requires a key prop--namely, one present
   * in an array literal or an arrow function expression.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-key.md
   */
  "react/jsx-key": [
    "error",
  ],

  /**
   * This option validates a specific depth for JSX.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-max-depth.md
   */
  "react/jsx-max-depth": [
    "off",
  ],

  /**
   * Limiting the maximum of props on a single line can improve readability.
   * 
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-max-props-per-line.md
   */
  "react/jsx-max-props-per-line": [
    "error",
    {
      maximum: 2,
      when: "always",
    }
  ],

  /**
   * A bind call or arrow function in a JSX prop will create a brand new function
   * on every single render. This is bad for performance, as it may cause
   * unnecessary re-renders if a brand new function is passed as a prop to a
   * component that uses reference equality check on the prop to determine if
   * it should update.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md
   */
  "react/jsx-no-bind": [
    "error",
    {
      allowArrowFunctions: true,
    }
  ],

  /**
   * This rule prevents comment strings (e.g. beginning with // or /*) from being
   * accidentally injected as a text node in JSX statements.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-comment-textnodes.md
   */
  "react/jsx-no-comment-textnodes": [
    "error",
  ],

  /**
   * Creating JSX elements with duplicate props can cause unexpected behavior
   * in your application.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-duplicate-props.md
   */
  "react/jsx-no-duplicate-props": [
    "error",
  ],

  /**
   * There are a couple of scenarios where you want to avoid string literals
   * in JSX. Either to enforce consistency and reducing strange behaviour, or
   * for enforcing that literals aren't kept in JSX so they can be translated.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-literals.md
   */
  "react/jsx-no-literals": [
    "error",
  ],

  /**
   * When creating a JSX element that has an a tag, it is often desired to have
   * the link open in a new tab using the target='_blank' attribute. Using this
   * attribute unaccompanied by rel='noreferrer noopener', however, is a severe
   * security vulnerability. This rules requires that you accompany target='_blank'
   * attributes with rel='noreferrer noopener'.
   *
   * Check https://mathiasbynens.github.io/rel-noopener/
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-target-blank.md
   */
  "react/jsx-no-target-blank": [
    "error",
  ],

  /**
   * This rule helps locate potential ReferenceErrors resulting from
   * misspellings or missing components.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-undef.md
   */
  "react/jsx-no-undef": [
    "error",
    {
      allowGlobals: true,
    },
  ],

  /**
   * This option limits every line in JSX to one expression each.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-one-expression-per-line.md
   */
  "react/jsx-one-expression-per-line": [
    "error",
    {
      allow: "literal",
    },
  ],

  /**
   * Enforces coding style that user-defined JSX components are defined and
   * referenced in PascalCase.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-pascal-case.md
   */
  "react/jsx-pascal-case": [
    "error",
  ],

  /**
   * Enforces that there is exactly one space between all attributes and after
   * tag name and the first attribute in the same line.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-props-no-multi-spaces.md
   */
  "react/jsx-props-no-multi-spaces": [
    "error",
  ],

  /**
   * Some developers prefer to sort defaultProps declarations alphabetically to
   * be able to find necessary declarations easier at a later time.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-default-props.md
   */
  "react/jsx-sort-default-props": [
    "error",
  ],

  /**
   * Some developers prefer to sort props names alphabetically to be able to
   * find necessary props easier at the later time. Others feel that it adds
   * complexity and becomes burden to maintain.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-props.md
   */
  "react/jsx-sort-props": [
    "error",
    {
      "callbacksLast": true,
    },
  ],

  /**
   * Enforce or forbid spaces after the opening bracket, before the closing
   * bracket, before the closing bracket of self-closing elements, and between
   * the angle bracket and slash of JSX closing or self-closing elements.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-tag-spacing.md
   */
  "react/jsx-tag-spacing": [
    "error",
    {
      closingSlash: "never",
      beforeSelfClosing: "always",
      afterOpening: "never",
    },
  ],

  /**
   * JSX expands to a call to React.createElement, a file which includes React
   * but only uses JSX should consider the React variable as used.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-uses-react.md
   */
  "react/jsx-uses-react": [
    "error",
  ],

  /**
   * Since 0.17.0 the ESLint no-unused-vars rule does not detect variables used
   * in JSX (see details). This rule will find variables used in JSX and mark
   * them as used.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-uses-vars.md
   */
  "react/jsx-uses-vars": [
    "error",
  ],

  /**
   * Wrapping multiline JSX in parentheses can improve readability and/or convenience.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-wrap-multilines.md
   */
  "react/jsx-wrap-multilines": [
    "error",
    {
      declaration: "parens-new-line",
      assignment: "parens-new-line",
      return: "parens-new-line",
      arrow: "parens-new-line",
      condition: "parens-new-line",
      logical: "parens-new-line",
      prop: "parens-new-line",
    },
  ],

  /**
   * This rule should prevent usage of this.state inside setState calls. Such
   * usage of this.state might result in errors when two state calls are called
   * in batch and thus referencing old state and not the current state.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-access-state-in-setstate.md
   */
  "react/no-access-state-in-setstate": [
    "off",
  ],

  /**
   * It's a bad idea to use the array index since it doesn't uniquely identify
   * your elements. In cases where the array is sorted or an element is added
   * to the beginning of the array, the index will be changed even though the
   * element representing that index may be the same. This results in unnecessary
   * renders.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-array-index-key.md
   */
  "react/no-array-index-key": [
    "error",
  ],

  /**
   * Children should always be actual children, not passed in as a prop.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-children-prop.md
   */
  "react/no-children-prop": [
    "error",
  ],

  /**
   * This rule helps prevent problems caused by using children and the
   * dangerouslySetInnerHTML prop at the same time. React will throw a warning
   * if this rule is ignored.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-danger-with-children.md
   */
  "react/no-danger-with-children": [
    "error",
  ],

  /**
   * Dangerous properties in React are those whose behavior is known to be a
   * common source of application vulnerabilities. The properties names clearly
   * indicate they are dangerous and should be avoided unless great care is taken.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-danger.md
   */
  "react/no-danger": [
    "error",
  ],

  /**
   * Several methods are deprecated between React versions. This rule will warn
   * you if you try to use a deprecated method. Use the shared settings to
   * specify the React version.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-deprecated.md
   */
  "react/no-deprecated": [
    "error",
  ],

  /**
   * Updating the state after a component mount will trigger a second render()
   * call and can lead to property/layout thrashing.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-did-mount-set-state.md
   */
  "react/no-did-mount-set-state": [
    "error",
  ],

  /**
   * Updating the state after a component update will trigger a second render()
   * call and can lead to property/layout thrashing.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-did-update-set-state.md
   */
  "react/no-did-update-set-state": [
    "error",
  ],

  /**
   * NEVER mutate this.state directly, as calling setState() afterwards may
   * replace the mutation you made. Treat this.state as if it were immutable.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-direct-mutation-state.md
   */
  "react/no-direct-mutation-state": [
    "error",
  ],

  /**
   * Facebook will eventually deprecate findDOMNode as it blocks certain
   * improvements in React in the future. It is recommended to use callback
   * refs instead.
   *
   * See https://github.com/yannickcr/eslint-plugin-react/issues/678#issue-165177220
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-find-dom-node.md
   */
  "react/no-find-dom-node": [
    "error",
  ],

  /**
   * isMounted is an anti-pattern, is not available when using ES6 classes, and it
   * is on its way to being officially deprecated.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-is-mounted.md
   */
  "react/no-is-mounted": [
    "error",
  ],

  /**
   * Declaring only one component per file improves readability and reusability
   * of components.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-multi-comp.md
   */
  "react/no-multi-comp": [
    "error",
    {
      ignoreStateless: true,
    }
  ],

  /**
   * Warns if you have shouldComponentUpdate defined when defining a component
   * that extends React.PureComponent. While having shouldComponentUpdate will
   * still work, it becomes pointless to extend PureComponent.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-redundant-should-component-update.md
   */
  "react/no-redundant-should-component-update": [
    "error",
  ],

  /**
   * This rule will warn you if you try to use the ReactDOM.render() return value.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-render-return-value.md
   */
  "react/no-render-return-value": [
    "error",
  ],

  /**
   * When using an architecture that separates your application state from your
   * UI components (e.g. Flux), it may be desirable to forbid the use of local
   * component state. This rule is especially helpful in read-only applications
   * (that don't use forms), since local component state should rarely be
   * necessary in such cases.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-set-state.md
   */
  "react/no-set-state": [
    "off",
  ],

  /**
   * Currently, two ways are supported by React to refer to components. The
   * first way, providing a string identifier, is now considered legacy in the
   * official documentation. The documentation now prefers a second method --
   * referring to components by setting a property on the this object in the
   * reference callback.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-string-refs.md
   */
  "react/no-string-refs": [
    "error",
  ],

  /**
   * When using a stateless functional component (SFC), props/context aren't
   * accessed in the same way as a class component or the create-react-class
   * format. Both props and context are passed as separate arguments to the
   * component instead. Also, as the name suggests, a stateless component does
   * not have state on this.state.
   *
   * Attempting to access properties on this can be a potential error if someone
   * is unaware of the differences when writing a SFC or missed when converting
   * a class component to a SFC.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-this-in-sfc.md
   */
  "react/no-this-in-sfc": [
    "error",
  ],

  /**
   * Ensure no casing typos were made declaring static class properties and
   * lifecycle methods. Checks that declared propTypes, contextTypes and
   * childContextTypes is supported by react-props.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-typos.md
   */
  "react/no-typos": [
    "error",
  ],

  /**
   * This rule prevents characters that you may have meant as JSX escape
   * characters from being accidentally injected as a text node in JSX statements.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unescaped-entities.md
   */
  "react/no-unescaped-entities": [
    "error",
  ],

  /**
   * In JSX all DOM properties and attributes should be camelCased to be
   * consistent with standard JavaScript style. This can be a possible source
   * of error if you are used to writing plain HTML.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unknown-property.md
   */
  "react/no-unknown-property": [
    "error",
  ],

  /**
   * Certain legacy lifecycle methods are unsafe for use in async React
   * applications and cause warnings in strict mode. These also happen to be
   * the lifecycles that cause the most confusion within the React community.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unsafe.md
   */
  "react/no-unsafe": [
    "error",
  ],

  /**
   * Warns you if you have defined a prop type but it is never being used anywhere.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unused-prop-types.md
   */
  "react/no-unused-prop-types": [
    "error",
  ],

  /**
   * Warns you if you have defined a property on the state, but it is not being
   * used anywhere.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unused-state.md
   */
  "react/no-unused-state": [
    "error",
  ],

  /**
   * Updating the state during the componentWillUpdate step can lead to
   * indeterminate component state and is not allowed.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-will-update-set-state.md
   */
  "react/no-will-update-set-state": [
    "error",
    "disallow-in-func",
  ],

  /**
   * React offers you two way to create traditional components: using the ES5
   * create-react-class module or the new ES6 class system. This rule allow you
   * to enforce one way or another.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-es6-class.md
   */
  "react/prefer-es6-class": [
    "error",
    "always",
  ],

  /**
   * Stateless functional components are simpler than class based components
   * and will benefit from future React performance optimizations specific to
   * these components.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-stateless-function.md
   */
  "react/prefer-stateless-function": [
    "error",
  ],

  /**
   * PropTypes improve the reusability of your component by validating the
   * received data.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prop-types.md
   */
  "react/prop-types": [
    "error",
  ],

  /**
   * When using JSX, <a /> expands to React.createElement("a"). Therefore the
   * React variable must be in scope.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/react-in-jsx-scope.md
   */
  "react/react-in-jsx-scope": [
    "error",
  ],

  /**
   * This rule aims to ensure that any non-required PropType declaration of a
   * component has a corresponding defaultProps value.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-default-props.md
   */
  "react/require-default-props": [
    "error",
    {
      forbidDefaultForRequired: true,
    },
  ],

  /**
   * This rule prevents you from creating React components without declaring a
   * shouldComponentUpdate method.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-optimization.md
   */
  "react/require-optimization": [
    "off",
  ],

  /**
   * When writing the render method in a component it is easy to forget to return
   * the JSX content. This rule will warn if the return statement is missing.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-render-return.md
   */
  "react/require-render-return": [
    "error",
  ],

  /**
   * Components without children can be self-closed to avoid unnecessary extra
   * closing tag.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/self-closing-comp.md
   */
  "react/self-closing-comp": [
    "error",
  ],

  /**
   * When creating React components it is more convenient to always follow the
   * same organisation for method order to help you easily find lifecycle methods,
   * event handlers, etc.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/sort-comp.md
   */
  "react/sort-comp": [
    "off", // This is not working right now with typescript. Check https://github.com/yannickcr/eslint-plugin-react/issues/2066
    {
      order: [
        "static-methods",
        "instance-variables",
        "getters",
        "setters",
        "lifecycle",
        "instance-methods",
        "render",
      ],
      groups: {
        lifecycle: [
          'constructor',
          'getDefaultProps',
          'getInitialState',
          'getChildContext',
          'getDerivedStateFromProps',
          'componentWillMount',
          'UNSAFE_componentWillMount',
          'componentDidMount',
          'componentWillReceiveProps',
          'UNSAFE_componentWillReceiveProps',
          'shouldComponentUpdate',
          'componentWillUpdate',
          'UNSAFE_componentWillUpdate',
          'getSnapshotBeforeUpdate',
          'componentDidUpdate',
          'componentDidCatch',
          'componentWillUnmount'
        ]
      }
    }
  ],

  /**
   * Some developers prefer to sort propTypes declarations alphabetically to be
   * able to find necessary declaration easier at the later time. Others feel
   * that it adds complexity and becomes burden to maintain.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/sort-prop-types.md
   */
  "react/sort-prop-types": [
    "error",
    {
      callbacksLast: true,
      sortShapeProp: true,
    }
  ],

  /**
   * Require that the value of the prop style be an object or a variable that
   * is an object.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/style-prop-object.md
   */
  "react/style-prop-object": [
    "error",
  ],

  /**
   * There are some HTML elements that are only self-closing (e.g. img, br, hr).
   * These are collectively known as void DOM elements. If you try to give these
   * children, React will give you a warning.
   *
   * https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/void-dom-elements-no-children.md
   */
  "react/void-dom-elements-no-children": [
    "error",
  ],
});

//#####################################################
// Vue Rules
//#####################################################
const vueRules = (opts) => ({

});

//#####################################################
// Typescript Rules
//#####################################################
const tsRules = (opts) => ({
  /**
   * Enforces function overloads to be consecutive. Improves readability and
   * organization by grouping naturally related items together.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/adjacent-overload-signatures.md
   */
  "@typescript-eslint/adjacent-overload-signatures": [
    "error",
  ],

  /**
   * Requires using either ‘T[]’ or ‘Array' for arrays.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/array-type.md
   */
  "@typescript-eslint/array-type": [
    "error",
    {
      default: "array",
    }
  ],

  /**
   * This rule disallows awaiting a value that is not a "Thenable" (an object which has then method, such as a Promise).
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/await-thenable.md
   */
  "@typescript-eslint/await-thenable": [
    "error",
  ],

  /**
   * Suppressing Typescript Compiler Errors can be hard to discover.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/ban-ts-ignore.md
   */
  "@typescript-eslint/ban-ts-ignore": [
    "error",
  ],

  /**
   * This rule bans specific types and can suggest alternatives. It does not ban the corresponding runtime objects from being used.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/ban-types.md
   */
  "@typescript-eslint/ban-types": [
    "off",
  ],

  /**
   * This rule extends the base eslint/brace-style rule. It supports all options and features of the base rule.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/brace-style.md
   */
  "@typescript-eslint/brace-style": [
    "error",
    "1tbs",
  ],

  /**
   * This rule looks for any underscores (_) located within the source code. It ignores leading and trailing underscores and only checks those in the middle of a variable name
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/camelcase.md
   */
  "@typescript-eslint/camelcase": [
    "error",
    {
      properties: "always",
      ignoreDestructuring: false,
    },
  ],

  /**
   * This rule enforces PascalCased names for classes and interfaces.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/class-name-casing.md
   */
  "@typescript-eslint/class-name-casing": [
    "error",
  ],

  /**
   * Type assertions are also commonly referred as "type casting" in TypeScript (even though it is technically slightly different to what is understood by type casting in other languages), so you can think of type assertions and type casting referring to the same thing. It is essentially you saying to the TypeScript compiler, "in this case, I know better than you!".
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/consistent-type-assertions.md
   */
  "@typescript-eslint/consistent-type-assertions": [
    "error",
    {
      assertionStyle: "as",
    }
  ],

  /**
   * There are two ways to define a type.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/consistent-type-definitions.md
   */
  "@typescript-eslint/consistent-type-definitions": [
    "error",
    "interface",
  ],

  /**
   * Explicit types for function return values makes it clear to any calling code what type is returned. This ensures that the return value is assigned to a variable of the correct type; or in the case where there is no return value, that the calling code doesn't try to use the undefined value when it shouldn't.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/explicit-function-return-type.md
   */
  "@typescript-eslint/explicit-function-return-type": [
    "off",
    {
      allowExpressions: false,
      allowTypedFunctionExpressions: true,
      allowHigherOrderFunctions: true,
    },
  ],

  /**
   * Leaving off accessibility modifier and making everything public can make your interface hard to use by others. If you make all internal pieces private or protected, your interface will be easier to use.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/explicit-member-accessibility.md
   */
  "@typescript-eslint/explicit-member-accessibility": [
    "error",
    {
      overrides: {
        accessors: "off",
        constructors: "off",
        methods: "explicit",
        properties: "explicit",
        parameterProperties: "off",
      }
    }
  ],

  /**
   * When calling a function, developers may insert optional whitespace between the function’s name and the parentheses that invoke it. This rule requires or disallows spaces between the function name and the opening parenthesis that calls it.
   * 
   * @extends eslint/func-call-spacing
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/func-call-spacing.md
   */
  "@typescript-eslint/func-call-spacing": [
    "error",
    "never",
  ],

  /**
   * It can be helpful to enforce a consistent naming style for generic type variables used within a type. For example, prefixing them with T and ensuring a somewhat descriptive name, or enforcing Hungarian notation.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/generic-type-naming.md
   */
  "@typescript-eslint/generic-type-naming": [
    "error",
    "^T[A-Za-z]*$",
  ],

  /**
   * There are several common guidelines which require specific indentation of nested blocks and statements.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/indent.md
   */
  "@typescript-eslint/indent": [
    "error",
    2,
    {
      SwitchCase: 1,
      MemberExpression: 1,
    }
  ],

  /**
   * This rule enforces whether or not the I prefix is required for interface names. The _ prefix is sometimes used to designate a private declaration, so the rule also supports a private interface that might be named _IAnimal instead of IAnimal.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/interface-name-prefix.md
   */
  "@typescript-eslint/interface-name-prefix": [
    "error",
    {
      prefixWithI: "always",
      allowUnderscorePrefix: false,
    }
  ],

  /**
   * Enforces a consistent member delimiter style in interfaces and type literals.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-delimiter-style.md
   */
  "@typescript-eslint/member-delimiter-style": [
    "error",
    {
      multiline: {
        delimiter: "semi",
        requireLast: true,
      },
      singleline: {
        delimiter: "semi",
        requireLast: false,
      }
    }
  ],

  /**
   * It can be helpful to enforce naming conventions for private (and sometimes protected) members of an object. For example, prefixing private properties with a _ allows them to be easily discerned when being inspected by tools that do not have knowledge of TypeScript (such as most debuggers).
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-naming.md
   */
  "@typescript-eslint/member-naming": [
    "error",
    {
      private: "^_",
      protected: "^_",
    }
  ],

  /**
   * A consistent ordering of fields, methods and constructors can make interfaces, type literals, classes and class expressions easier to read, navigate and edit.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-ordering.md
   */
  "@typescript-eslint/member-ordering": [
    "error",
    {
      default: [
        "private-static-field",
        "public-static-field",
        "private-instance-field",
        "public-instance-field",
        "private-constructor",
        "public-constructor",
        "private-instance-method",
        "protected-instance-method",
        "public-instance-method",
      ]
    }
  ],

  /**
   * Use of the Array constructor to construct a new array is generally discouraged in favor of array literal notation because of the single-argument pitfall and because the Array global may be redefined. Two exceptions are when the Array constructor is used to intentionally create sparse arrays of a specified size by giving the constructor a single numeric argument, or when using TypeScript type parameters to specify the type of the items of the array (new Array<Foo>()).
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-array-constructor.md
   */
  "@typescript-eslint/no-array-constructor": [
    "error",
  ],

  /**
   * Deleting dynamically computed keys can be dangerous and in some cases not well optimized.
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-dynamic-delete.md
   */
  "@typescript-eslint/no-dynamic-delete": [
    "error",
  ],

  /**
   * Empty functions can reduce readability because readers need to guess whether it’s intentional or not. So writing a clear comment for empty functions is a good practice.
   * 
   * @extends eslint/no-empty-function
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-empty-function.md
   */
  "@typescript-eslint/no-empty-function": [
    "error",
  ],

  /**
   * Using the any type defeats the purpose of using TypeScript. When any is used, all compiler type checks around that value are ignored.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-explicit-any.md
   */
  "@typescript-eslint/no-explicit-any": [
    "off",
  ],

  /**
   * Disallow extra non-null assertion.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-extra-non-null-assertion.md
   */
  "@typescript-eslint/no-extra-non-null-assertion": [
    "error",
  ],

  /**
   * This rule restricts the use of parentheses to only where they are necessary.
   * 
   * @extends eslint/no-extra-parens
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-extra-parens.md
   */
  "@typescript-eslint/no-extra-parens": [
    "error",
    "all",
    {
      conditionalAssign: true,
      nestedBinaryExpressions: false,
      returnAssign: false,
      ignoreJSX: "all",
      enforceForArrowConditionals: false,
    },
  ],

  /**
   * This rule warns when a class is accidentally used as a namespace.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-extraneous-class.md
   */
  "@typescript-eslint/no-extraneous-class": [
    "error",
  ],

  /**
   * This rule forbids usage of Promise-like values in statements without handling their errors appropriately. Unhandled promises can cause several issues, such as improperly sequenced operations, ignored Promise rejections and more. Valid ways of handling a Promise-valued statement include awaiting, returning, and either calling .then() with two arguments or .catch() with one argument.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-floating-promises.md
   */
  "@typescript-eslint/no-floating-promises": [
    "off",
  ],

  /**
   * A for-in loop (for (var k in o)) iterates over the properties of an Object. While it is legal to use for-in loops with array types, it is not common. for-in will iterate over the indices of the array as strings, omitting any "holes" in the array. More common is to use for-of, which iterates over the values of an array. 
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-for-in-array.md
   */
  "@typescript-eslint/no-for-in-array": [
    "error",
  ],

  /**
   * Explicit types where they can be easily inferred may add unnecessary verbosity.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-inferrable-types.md
   */
  "@typescript-eslint/no-inferrable-types": [
    "error",
  ],

  /**
   * 'Magic numbers' are numbers that occur multiple times in code without an explicit meaning. They should preferably be replaced by named constants.
   * 
   * @extends eslint/no-magic-numbers
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-magic-numbers.md
   */
  "@typescript-eslint/no-magic-numbers": [
    "error",
    {
      ignoreNumericLiteralTypes: true,
      ignoreReadonlyClassProperties: true,
      ignoreEnums: true,
      ignoreArrayIndexes: true,
      enforceConst: true,
      detectObjects: false,
      ignore: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    }
  ],

  /**
   * Warns on apparent attempts to define constructors for interfaces or new for classes.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-misused-new.md
   */
  "@typescript-eslint/no-misused-new": [
    "error",
  ],

  /**
   * This rule forbids using promises in places where the Typescript compiler allows them but they are not handled properly. These situations can often arise due to a missing await keyword or just a misunderstanding of the way async functions are handled/awaited.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-misused-promises.md
   */
  "@typescript-eslint/no-misused-promises": [
    "error",
    {
      checksConditionals: true,
      checksVoidReturn: true,
    }
  ],

  /**
   * Custom TypeScript modules (module foo {}) and namespaces (namespace foo {}) are considered outdated ways to organize TypeScript code. ES2015 module syntax is now preferred (import/export).
   * This rule still allows the use of TypeScript module declarations to describe external APIs (declare module 'foo' {}).
   *
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-namespace.md
   */
  "@typescript-eslint/no-namespace": [
    "error",
    {
      allowDeclarations: true,
      allowDefinitionFiles: true,
    }
  ],

  /**
   * Using non-null assertions cancels the benefits of the strict null-checking mode.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-non-null-assertion.md
   */
  "@typescript-eslint/no-non-null-assertion": [
    "error",
  ],

  /**
   * Parameter properties can be confusing to those new to TypeScript as they are less explicit than other ways of declaring and initializing class members.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-parameter-properties.md
   */
  "@typescript-eslint/no-parameter-properties": [
    "error",
  ],

  /**
   * Prefer the newer ES6-style imports over require().
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-require-imports.md
   */
  "@typescript-eslint/no-require-imports": [
    "error",
  ],

  /**
   * This rule prohibits assigning variables to this.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-this-alias.md
   */
  "@typescript-eslint/no-this-alias": [
    "error",
    {
      allowDestructuring: true, // Allow `const { props, state } = this`; false by default
      allowedNames: ["self"], // Allow `const self = this`; `[]` by default
    },
  ],

  /**
   * This rule disallows the use of type aliases in favor of interfaces and simplified types (primitives, tuples, unions, intersections, etc).
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-type-alias.md
   */
  "@typescript-eslint/no-type-alias": [
    "off",
  ],

  /**
   * Any expression being used as a condition must be able to evaluate as truthy or falsy in order to be considered "necessary".
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-condition.md
   */
  "@typescript-eslint/no-unnecessary-condition": [
    "off",
    {
      ignoreRhs: true,
      allowConstantLoopConditions: true,
    }
  ],

  /**
   * This rule aims to let users know when a namespace or enum qualifier is unnecessary, whether used for a type or for a value.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-qualifier.md
   */
  "@typescript-eslint/no-unnecessary-qualifier": [
    "error",
  ],

  /**
   * Warns if an explicitly specified type argument is the default for that type parameter.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-type-arguments.md
   */
  "@typescript-eslint/no-unnecessary-type-arguments": [
    "error",
  ],

  /**
   * This rule prohibits using a type assertion that does not change the type of an expression.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-type-assertion.md
   */
  "@typescript-eslint/no-unnecessary-type-assertion": [
    "error",
  ],

  /**
   * This rule aims to eliminate unused expressions which have no effect on the state of the program.
   * 
   * @extends eslint/no-unused-expressions
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-expressions.md
   */
  "@typescript-eslint/no-unused-expressions": [
    "error",
  ],

  /**
   * This rule aims to ensure that only typed public methods are declared in the code.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-untyped-public-signature.md
   */
  "@typescript-eslint/no-untyped-public-signature": [
    "off",
  ],

  /**
   * This rule is aimed at eliminating unused variables, functions, and parameters of functions.
   * 
   * @extends eslint/no-unused-vars
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-vars.md
   */
  "@typescript-eslint/no-unused-vars": [
    "error",
    {
      vars: "all",
      varsIgnorePattern: "^_",
      argsIgnorePattern: "^_",
      args: "after-used",
      ignoreRestSiblings: false,
    },
  ],

  /**
   * This rule leverages the TypeScript compiler's unused variable checks to report. This means that with all rule options set to false, it should report the same errors as if you used both the noUnusedLocals and noUnusedParameters compiler options.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-vars-experimental.md
   */
  "@typescript-eslint/no-unused-vars-experimental": [
    "error",
  ],

  /**
   * This rule will warn when it encounters a reference to an identifier that has not yet been declared.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-use-before-define.md
   */
  "@typescript-eslint/no-use-before-define": [
    "error",
    {
      functions: true,
      classes: true,
    }
  ],

  /**
   * This rule flags class constructors that can be safely removed without changing how the class works.
   * 
   * @extends eslint/no-useless-constructor
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-useless-constructor.md
   */
  "@typescript-eslint/no-useless-constructor": [
    "error",
  ],

  /**
   * Disallows the use of require statements except in import statements.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-var-requires.md
   */
  "@typescript-eslint/no-var-requires": [
    "error",
  ],

  /**
   * This rule recommends a for-of loop when the loop index is only used to read from an array that is being iterated.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-for-of.md
   */
  "@typescript-eslint/prefer-for-of": [
    "off",
  ],

  /**
   * This rule suggests using a function type instead of an interface or object type literal with a single call signature.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-function-type.md
   */
  "@typescript-eslint/prefer-function-type": [
    "error",
  ],

  /**
   * This rule is aimed at suggesting includes method if indexOf method was used to check whether an object contains an arbitrary value or not.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-includes.md
   */
  "@typescript-eslint/prefer-includes": [
    "error",
  ],

  /**
   * This rule aims to standardise the way modules are declared.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-namespace-keyword.md
   */
  "@typescript-eslint/prefer-namespace-keyword": [
    "error",
  ],

  /**
   * This rule aims enforce the usage of the safer operator.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-nullish-coalescing.md
   */
  "@typescript-eslint/prefer-nullish-coalescing": [
    "error",
  ],

  /**
   * This rule aims enforce the usage of the safer operator.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-optional-chain.md
   */
  "@typescript-eslint/prefer-optional-chain": [
    "off",
  ],

  /**
   * This rule enforces that private members are marked as readonly if they're never modified outside of the constructor.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-readonly.md
   */
  "@typescript-eslint/prefer-readonly": [
    "error",
  ],

  /**
   * RegExp#exec is faster than String#match and both work the same when not using the /g flag.
   * This rule is aimed at enforcing the more performant way of applying regular expressions on strings.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-regexp-exec.md
   */
  "@typescript-eslint/prefer-regexp-exec": [
    "error",
  ],

  /**
   * This rule is aimed at enforcing a consistent way to check whether a string starts or ends with a specific string.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-string-starts-ends-with.md
   */
  "@typescript-eslint/prefer-string-starts-ends-with": [
    "error",
  ],

  /**
   * Requires any function or method that returns a Promise to be marked async. 
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/promise-function-async.md
   */
  "@typescript-eslint/promise-function-async": [
    "error",
    {
      "allowedPromiseNames": ["Thenable"],
      "checkArrowFunctions": false,
      "checkFunctionDeclarations": true,
      "checkFunctionExpressions": true,
      "checkMethodDeclarations": true
    }
  ],

  /**
   * This rule extends the base eslint/quotes rule. It supports all options and features of the base rule.
   * 
   * @extends eslint/quotes
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/quotes.md
   */
  "@typescript-eslint/quotes": [
    "error",
    "double",
    {
      allowTemplateLiterals: true,
    },
  ],

  /**
   * This rule prevents to invoke Array#sort() method without compare argument.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/require-array-sort-compare.md
   */
  "@typescript-eslint/require-array-sort-compare": [
    "error",
  ],

  /**
   * Asynchronous functions that don’t use await might not need to be asynchronous functions and could be the unintentional result of refactoring.
   * 
   * @extends eslint/require-await
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/require-await.md
   */
  "@typescript-eslint/require-await": [
    "off", // Should be turned on
  ],

  /**
   * When adding two variables, operands must both be of type number or of type string.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/restrict-plus-operands.md
   */
  "@typescript-eslint/restrict-plus-operands": [
    "error",
  ],

  /**
   * Enforce template literal expressions to be of string type.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/restrict-template-expressions.md
   */
  "@typescript-eslint/restrict-template-expressions": [
    "off",
  ],

  /**
   * Returning an awaited promise can make sense for better stack trace information as well as for consistent error handling (returned promises will not be caught in an async function try/catch).
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/return-await.md
   */
  "@typescript-eslint/return-await": [
    "error",
    "in-try-catch",
  ],

  /**
   * This rule enforces consistent use of semicolons after statements.
   * 
   * @extends eslint/semi
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/semi.md
   */
  "@typescript-eslint/semi": [
    "error",
    "always",
  ],

  /**
   * This rule extends the base eslint/func-call-spacing rule. It supports all options and features of the base rule. This version adds support for generic type parameters on function calls.
   * 
   * @extends eslint/space-before-function-paren
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/space-before-function-paren.md
   */
  "@typescript-eslint/space-before-function-paren": [
    "error",
    {
      anonymous: "never",
      named: "never",
      asyncArrow: "always",
    },
  ],

  /**
   * Requires that any boolean expression is limited to true booleans rather than casting another primitive to a boolean at runtime.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/strict-boolean-expressions.md
   */
  "@typescript-eslint/strict-boolean-expressions": [
    "off",
  ],

  /**
   * Use of triple-slash reference type directives is discouraged in favor of the newer import style. This rule allows you to ban use of /// <reference path="" />, /// <reference types="" />, or /// <reference lib="" /> directives.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/triple-slash-reference.md
   */
  "@typescript-eslint/triple-slash-reference": [
    "error",
  ],

  /**
   * This rule aims to enforce specific spacing patterns around type annotations and function types in type literals.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/type-annotation-spacing.md
   */
  "@typescript-eslint/type-annotation-spacing": [
    "error",
  ],

  /**
   * This rule can enforce type annotations in locations regardless of whether they're required. This is typically used to maintain consistency for element types that sometimes require them.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/typedef.md
   */
  "@typescript-eslint/typedef": [
    "off",
    {
      arrayDestructuring: false,
      arrowParameter: true,
      memberVariableDeclaration: true,
      objectDestructuring: false,
      parameter: true,
      propertyDeclaration: true,
      variableDeclaration: false,
    }
  ],

  /**
   * Warns when a method is used outside of a method call.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/unbound-method.md
   */
  "@typescript-eslint/unbound-method": [
    "off",
  ],

  /**
   * This rule aims to keep the source code as maintanable as posible by reducing the amount of overloads.
   * 
   * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/unified-signatures.md
   */
  "@typescript-eslint/unified-signatures": [
    "error",
  ]
});

//#####################################################
// Eslint config
//#####################################################
const eslintConfig = (opts) => ({
  env: {
    es6: true,
    node: true
  },

  extends: [
    "plugin:import/typescript",
  ],

  plugins: [
    "import",
    "jsdoc",
    "react",
    "@typescript-eslint"
  ],

  parser: "@typescript-eslint/parser",

  parserOptions: {
    ...(opts && opts.parserOptions ? opts.parserOptions : {}),
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    }
  },

  settings: {
    "import/resolver": {
      node: {
        extensions: [
          ".js",
          ".jsx",
          ".ts",
          ".tsx"
        ],
        moduleDirectory: [
          "node_modules",
          "./src"
        ]
      }
    },

    /**
     * For some reason nodejs built in modules are not
     * being recognized so we have to add them by hand.
     */
    "import/core-modules": builtinModules,

    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx"]
    },

    "react": {
      "createClass": "createReactClass",
      "pragma": "React",
      "version": "16.6",
    },

    "propWrapperFunctions": [
      "forbidExtraProps",
      { "property": "freeze", "object": "Object" },
      { "property": "myFavoriteWrapper" }
    ],
  },

  overrides: [
    {
      "files": ["*.js", "*.jsx"],
      "rules": {
        "import/no-commonjs": "off",
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/no-require-imports": "off",
      }
    }
  ]
});

//#####################################################
// Config builder
//#####################################################
const configBuild = function(config = {}) {
  this.eslintRules = eslintRules;

  let configClone = lodash.cloneDeep(config);
  let opts = configClone.configBuilderOptions || {};

  // Delete options from top level config.
  delete configClone.configBuilderOptions;

  // Compile other rules.
  let tsRulesClone = tsRules(opts);
  let eslintRulesClone = eslintRules(opts);
  let eslintConfigClone = eslintConfig(opts);

  if (opts.tsRules) {
    tsRulesClone = lodash.merge(tsRulesClone, opts.tsRules);
  }

  if (opts.tsConfigFile) {
    eslintRulesClone = Object.assign(
      {},
      eslintRulesClone,
      tsRulesClone
    );

    eslintConfigClone.parserOptions.project = opts.tsConfigFile;

    if (!opts.webpackConfigFile) {
      eslintConfigClone.settings["import/resolver"] = {
        "typescript": {
          "directory": opts.tsConfigFile,
        }
      };
    }
  }

  if(opts.webpackConfigFile) {
    eslintConfigClone.settings["import/resolver"] = {
      webpack: {
        config: opts.webpackConfigFile
      }
    };
  }

  // Set eslint rules to config.
  eslintConfigClone.rules = lodash.merge(
    {},
    eslintRulesClone,
    importRules(opts),
    jsdocRules(opts),
    reactRules(opts),
  );

  // User config should take precedence.
  eslintConfigClone = lodash.merge(
    eslintConfigClone,
    configClone,
  );

  // Set plugins
  if (config.plugins) {
    eslintConfigClone.plugins = eslintConfig.plugins.concat(config.plugins);
  }

  // Return the config
  return eslintConfigClone;
};

//#####################################################
// Export
//#####################################################
module.exports = {
  eslintRules,
  configBuild
};
