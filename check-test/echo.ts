/**
 * A hellow function.
 */
function hello() {
  console.log("hellow"); // eslint-disable-line
}

/**
 * A hi function.
 */
function hi() {
  console.log("hi"); // eslint-disable-line
}

// Export
export {
  hello,
  hi,
};
