import EventsEmitter from "events";

/**
 * A cole function.
 */
function cole() {
  console.log("cole"); // eslint-disable-line
  new EventsEmitter();
}

/**
 * A cole function.
 */
function manolo() {
  console.log("manolo"); // eslint-disable-line
}

// Export
export {
  cole,
  manolo,
};
